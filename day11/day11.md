# AoC 2020 day 11
Looks a bit like a strange variant of the game of life. Let's see: the heart of the solution will be a function that counts neighbouring occupied seats for a given seat map and location. The map is a 2-dim array of char. Yes, this time not an array of arrays, but a true 2-dim array, I have to learn how to use them as well.


```julia
function nocc(mapa,x,y)
    maxx, maxy = size(mapa)
    noc = 0
    for i = maximum((x-1,1)):minimum((x+1,maxx)), j = maximum((y-1,1)):minimum((y+1,maxy))
        noc += mapa[i,j] == '#'
    end
    return noc - (mapa[x,y] == '#')
end
```




    nocc (generic function with 1 method)



The following function converts a list of lines into a 2-dim array. Assume all the lines are same length.


```julia
function makemap(l)
    nc = length(l[1])
    nl = length(l)
    m = [l[i][j] for i=1:nl, j=1:nc]
end
t = makemap(["LLL",".#L","L##"])
```




    3×3 Array{Char,2}:
     'L'  'L'  'L'
     '.'  '#'  'L'
     'L'  '#'  '#'



Some "unit tests" for the `nocc` function


```julia
println("$(nocc(t,1,1)), $(nocc(t,2,2)), $(nocc(t,3,2)), $(nocc(t,2,3))") 
```

    1, 2, 2, 3
    

Looks OK, except that Julia apparently uses `(row,col)` ordering of array indices. Here we don't really care, but the standard convention for 2-dim arrays in AoC up till now was '`x` runs to the right, `y` runs down'. Have to remember that.

Now write a function that transforms a map into a "next step" map.


```julia
function transf(mapa)
    function tcell(x,y)      # function that transforms a single cell
        if mapa[x,y] == '.'
            return '.'              # floor stays floor, no matter what
        elseif mapa[x,y] == 'L'
            if nocc(mapa,x,y) == 0
                return '#'
            else
                return 'L'
            end
        else
            if nocc(mapa,x,y) ≥ 4
                return 'L'
            else
                return '#'
            end
        end
    end
    [tcell(i,j) for i=1:size(mapa,1), j=1:size(mapa,2)]
end
function readmap(file)
    f = open(file)
    l = readlines(f)
    close(f)
    makemap(l)
end
testmap = readmap("test.txt")
```




    10×10 Array{Char,2}:
     'L'  '.'  'L'  'L'  '.'  'L'  'L'  '.'  'L'  'L'
     'L'  'L'  'L'  'L'  'L'  'L'  'L'  '.'  'L'  'L'
     'L'  '.'  'L'  '.'  'L'  '.'  '.'  'L'  '.'  '.'
     'L'  'L'  'L'  'L'  '.'  'L'  'L'  '.'  'L'  'L'
     'L'  '.'  'L'  'L'  '.'  'L'  'L'  '.'  'L'  'L'
     'L'  '.'  'L'  'L'  'L'  'L'  'L'  '.'  'L'  'L'
     '.'  '.'  'L'  '.'  'L'  '.'  '.'  '.'  '.'  '.'
     'L'  'L'  'L'  'L'  'L'  'L'  'L'  'L'  'L'  'L'
     'L'  '.'  'L'  'L'  'L'  'L'  'L'  'L'  '.'  'L'
     'L'  '.'  'L'  'L'  'L'  'L'  'L'  '.'  'L'  'L'




```julia
b = transf(testmap)
```




    10×10 Array{Char,2}:
     '#'  '.'  '#'  '#'  '.'  '#'  '#'  '.'  '#'  '#'
     '#'  '#'  '#'  '#'  '#'  '#'  '#'  '.'  '#'  '#'
     '#'  '.'  '#'  '.'  '#'  '.'  '.'  '#'  '.'  '.'
     '#'  '#'  '#'  '#'  '.'  '#'  '#'  '.'  '#'  '#'
     '#'  '.'  '#'  '#'  '.'  '#'  '#'  '.'  '#'  '#'
     '#'  '.'  '#'  '#'  '#'  '#'  '#'  '.'  '#'  '#'
     '.'  '.'  '#'  '.'  '#'  '.'  '.'  '.'  '.'  '.'
     '#'  '#'  '#'  '#'  '#'  '#'  '#'  '#'  '#'  '#'
     '#'  '.'  '#'  '#'  '#'  '#'  '#'  '#'  '.'  '#'
     '#'  '.'  '#'  '#'  '#'  '#'  '#'  '.'  '#'  '#'




```julia
c = transf(b)
```




    10×10 Array{Char,2}:
     '#'  '.'  'L'  'L'  '.'  'L'  '#'  '.'  '#'  '#'
     '#'  'L'  'L'  'L'  'L'  'L'  'L'  '.'  'L'  '#'
     'L'  '.'  'L'  '.'  'L'  '.'  '.'  'L'  '.'  '.'
     '#'  'L'  'L'  'L'  '.'  'L'  'L'  '.'  'L'  '#'
     '#'  '.'  'L'  'L'  '.'  'L'  'L'  '.'  'L'  'L'
     '#'  '.'  'L'  'L'  'L'  'L'  '#'  '.'  '#'  '#'
     '.'  '.'  'L'  '.'  'L'  '.'  '.'  '.'  '.'  '.'
     '#'  'L'  'L'  'L'  'L'  'L'  'L'  'L'  'L'  '#'
     '#'  '.'  'L'  'L'  'L'  'L'  'L'  'L'  '.'  'L'
     '#'  '.'  '#'  'L'  'L'  'L'  'L'  '.'  '#'  '#'



Tests OK, solve the problem, i.e. iterate until the map is stable and count occupied seats.


```julia
function sol1(mapa)
    while (next=transf(mapa)) ≠ mapa
        mapa = next
    end
    count(c->c=='#',mapa)
end
sol1(testmap)
```




    37



In agreement with the example given by puzzle author. So, read the competition data in and calculate the answer:


```julia
realmap = readmap("input.txt")
sol1(realmap)
```




    2126



## Part 2
We have to change the `nocc` function, that would be the largest change to make. The function `transf` can stay, just make it use the new occupation count function, and change the number of occupied seats that cause a seat to become free. Should have made them both function parameters... will do now. Pity that we don't know the second puzzle of the day before solving the first one and can't plan such things beforehand.

As for changes in `nocc`, fortunately we have to implement the line-of-sight only in eight directions, not omnidirectionally. Define a step in each direction as a pair of numbers and walk in that direction until hitting an obstacle or a wall. 


```julia
function nocc2(mapa,x,y)
    maxx, maxy = size(mapa)
    step = [(-1,-1),(-1,0),(-1,1),(0,-1),(0,1),(1,-1),(1,0),(1,1)]
    noc = 0
    for s in step   # Loop over directions
        xx,yy = (x,y) .+ s
        while xx in 1:maxx && yy in 1:maxy   # This could be probably written in a cleaner way  
            if mapa[xx,yy] == '#'
                noc += 1
                break
            elseif mapa[xx,yy] == 'L'
                break
            end
            xx,yy = (xx,yy) .+ s
        end
    end
    return noc
end
```




    nocc2 (generic function with 1 method)



Some test are in order


```julia
testd2 = readmap("test2.txt")
```




    9×9 Array{Char,2}:
     '.'  '.'  '.'  '.'  '.'  '.'  '.'  '#'  '.'
     '.'  '.'  '.'  '#'  '.'  '.'  '.'  '.'  '.'
     '.'  '#'  '.'  '.'  '.'  '.'  '.'  '.'  '.'
     '.'  '.'  '.'  '.'  '.'  '.'  '.'  '.'  '.'
     '.'  '.'  '#'  'L'  '.'  '.'  '.'  '.'  '#'
     '.'  '.'  '.'  '.'  '#'  '.'  '.'  '.'  '.'
     '.'  '.'  '.'  '.'  '.'  '.'  '.'  '.'  '.'
     '#'  '.'  '.'  '.'  '.'  '.'  '.'  '.'  '.'
     '.'  '.'  '.'  '#'  '.'  '.'  '.'  '.'  '.'




```julia
nocc2(testd2,5,4)
```




    8




```julia
testd3=readmap("test3.txt")
```




    3×13 Array{Char,2}:
     '.'  '.'  '.'  '.'  '.'  '.'  '.'  '.'  '.'  '.'  '.'  '.'  '.'
     '.'  'L'  '.'  'L'  '.'  '#'  '.'  '#'  '.'  '#'  '.'  '#'  '.'
     '.'  '.'  '.'  '.'  '.'  '.'  '.'  '.'  '.'  '.'  '.'  '.'  '.'




```julia
nocc2(testd3,2,2)
```




    0




```julia
testd4=readmap("test4.txt")
```




    7×7 Array{Char,2}:
     '.'  '#'  '#'  '.'  '#'  '#'  '.'
     '#'  '.'  '#'  '.'  '#'  '.'  '#'
     '#'  '#'  '.'  '.'  '.'  '#'  '#'
     '.'  '.'  '.'  'L'  '.'  '.'  '.'
     '#'  '#'  '.'  '.'  '.'  '#'  '#'
     '#'  '.'  '#'  '.'  '#'  '.'  '#'
     '.'  '#'  '#'  '.'  '#'  '#'  '.'




```julia
nocc2(testd4,4,4)
```




    0



All OK. And the announced small change to `transf`


```julia
function transf(mapa,fnoc,nmax)
    function tcell(x,y)
        if mapa[x,y] == '.'
            return '.'
        elseif mapa[x,y] == 'L'
            if fnoc(mapa,x,y) == 0
                return '#'
            else
                return 'L'
            end
        else
            if fnoc(mapa,x,y) ≥ nmax
                return 'L'
            else
                return '#'
            end
        end
    end
    [tcell(i,j) for i=1:size(mapa,1), j=1:size(mapa,2)]
end
```




    transf (generic function with 2 methods)




```julia
b = transf(testmap,nocc2,5)
```




    10×10 Array{Char,2}:
     '#'  '.'  '#'  '#'  '.'  '#'  '#'  '.'  '#'  '#'
     '#'  '#'  '#'  '#'  '#'  '#'  '#'  '.'  '#'  '#'
     '#'  '.'  '#'  '.'  '#'  '.'  '.'  '#'  '.'  '.'
     '#'  '#'  '#'  '#'  '.'  '#'  '#'  '.'  '#'  '#'
     '#'  '.'  '#'  '#'  '.'  '#'  '#'  '.'  '#'  '#'
     '#'  '.'  '#'  '#'  '#'  '#'  '#'  '.'  '#'  '#'
     '.'  '.'  '#'  '.'  '#'  '.'  '.'  '.'  '.'  '.'
     '#'  '#'  '#'  '#'  '#'  '#'  '#'  '#'  '#'  '#'
     '#'  '.'  '#'  '#'  '#'  '#'  '#'  '#'  '.'  '#'
     '#'  '.'  '#'  '#'  '#'  '#'  '#'  '.'  '#'  '#'




```julia
c = transf(b,nocc2,5)
```




    10×10 Array{Char,2}:
     '#'  '.'  'L'  'L'  '.'  'L'  'L'  '.'  'L'  '#'
     '#'  'L'  'L'  'L'  'L'  'L'  'L'  '.'  'L'  'L'
     'L'  '.'  'L'  '.'  'L'  '.'  '.'  'L'  '.'  '.'
     'L'  'L'  'L'  'L'  '.'  'L'  'L'  '.'  'L'  'L'
     'L'  '.'  'L'  'L'  '.'  'L'  'L'  '.'  'L'  'L'
     'L'  '.'  'L'  'L'  'L'  'L'  'L'  '.'  'L'  'L'
     '.'  '.'  'L'  '.'  'L'  '.'  '.'  '.'  '.'  '.'
     'L'  'L'  'L'  'L'  'L'  'L'  'L'  'L'  'L'  '#'
     '#'  '.'  'L'  'L'  'L'  'L'  'L'  'L'  '.'  'L'
     '#'  '.'  'L'  'L'  'L'  'L'  'L'  '.'  'L'  '#'



After changing parameters to `transf` the solution function becomes


```julia
function sol2(mapa)
    while (next=transf(mapa,nocc2,5)) ≠ mapa
        mapa = next
    end
    count(c->c=='#',mapa)
end
sol2(testmap)
```




    26




```julia
sol2(realmap)
```




    1914



## More
Competition done, now try to learn something new about Julia. Fist: there exists a handy `axes` function that produces ready ranges for array indices. Using it I could drop the `maxx` and `maxy` variables and write the loop condition in the `nocc2` function as `while xx in axes(mapa,1) && yy in axes(mapa,2)`.

Moreover, `axes` can return a tuple of ranges for multidimensional arrays


```julia
axes(realmap)
```




    (Base.OneTo(90), Base.OneTo(92))



This gives a way to check that a tuple of indices points to a valid array element


```julia
function inarr(array,x,y)
    all((x,y) .∈ axes(array))
end
(inarr(testmap,2,9), inarr(testmap,0,7), inarr(testmap,1,11))
```




    (true, false, false)



A second interesting observation is that you can intersect ranges like sets and get a range as a result.


```julia
(1:7) ∩ (4:10)
```




    4:7



Which allows to write the `nocc` function from part one as


```julia
function nocc(mapa,x,y)
    noc = 0
    for i = ((x-1):(x+1)) ∩ axes(mapa,1), j = ((y-1):(y+1)) ∩ axes(mapa,2)
        noc += mapa[i,j] == '#'
    end
    noc - (mapa[x,y] == '#')
end
```




    nocc (generic function with 1 method)




```julia
(nocc(t,1,1), nocc(t,2,2), nocc(t,3,2), nocc(t,2,3)) 
```




    (1, 2, 2, 3)



Just like the original `nocc`. A trick worth remembering.

The last thing I learned is how to assemble a string from an array of characters. First row of my competition data looks like this (note `String` with capital 'S', it is a constructor, not a function).


```julia
String(realmap[1,:])
```




    "LLLLL.L.LLLLLLL.LL.LLLLL.LLLLLLL..LLLLL.LLLLL.L..LLLLL..LLLLLLLL.LLLL.LLL.LLLLLLL.LLLLLLLLLL"



And the last column is


```julia
String(realmap[:,end])
```




    "LLLLLLLLLLLLLLLLLLLL.LLL.LLLLLLLLLLLLLLLLL.LLLLLLLL.LLLLLLLLLLLLLLLLL..LLLLLLLL.L.LLLLL.LL"


