# AoC 2020 day 23
The reflex reaction is "use a list, inserting/deleting elements as necessary", but it that the best solution? Insertions and deletions may be costly in terms of computing time. Hmmm, there is a `CircularArrays` package, maybe should try that? Just swap array elements in place and never worry about indices wrapping around?


```julia
using CircularArrays
function game(initconf,nround)
    # prepare initial configuratoon
    initarr = parse.(Int,split(initconf,""))
    a = CircularArray{Int}(initarr)
    cc = 1     # Index of the "current cup"
    for n = 1:nround
        e3 = a[(cc+1):(cc+3)] # Three "picked up" cups
        dc = 0            # Index of "destination cup"
        dcn = a[cc]
        while dc == 0
            dcn = dcn-1
            if dcn == 0
                dcn = 9
            end
            for i in (cc+4):(cc+9) # I don't ike that linear search, but for the first part it is ok
                if a[i] == dcn
                    dc = i
                end
            end
        end
        len = dc-cc-3   
        a[(cc+1):(cc+len)] = a[(cc+4):(cc+len+3)]
        a[(cc+len+1):(cc+len+3)]=e3
        cc += 1
    end
    i1 = findfirst(isequal(1),a)
    println(join(string.(a[(i1+1):(i1+8)]),""))
end
game("389125467",100)
```

    67384529
    

And on my input


```julia
game("589174263",100)
```

    43896725
    

## Part 2
Such a nice idea and have to throw it away, the time needed for linear searches and copying slices will kill me. Need [something completely different™](https://youtu.be/u0WOIwlXE9g?t=145). Something like a two-way dictionary. Can be implemented using two arrays: one holds, for each cup, its position, the other one, for each position, the cup at that position. 


```julia
function game2(initconf,size,nround)
    cup1 = zeros(Int,size)    # which cup is at position
    pos = zeros(Int,size)    # which position has cup
    initarr = parse.(Int,split(initconf,""))
    for i in 1:length(initarr)
        cup1[i]=initarr[i]
        pos[initarr[i]] = i
    end
    for i in (length(initarr)+1):size
        cup1[i] = i
        pos[i] = i
    end
    cup = CircularArray{Int}(cup1)
    cpos = 1
    for n = 1:nround
        e3 = cup[(cpos+1):(cpos+3)]
        dpos = 0
        dcup = cup[cpos]
        while dpos == 0
            dcup = dcup-1
            if dcup < 1
                dcup = size
            end
            if !(dcup in e3)
                dpos = pos[dcup]
            end
        end
        # now everything from position cc+4 to dpos has to be shifted 3 positions left
        for i = (cpos+4):dpos
            cup[i-3] = cup[i]
            pos[cup[i]] = i-3
        end
        # and put the 3 cups after that
        for i = 1:3
            cup[dpos-3+i] = e3[i]
            pos[e3[i]] = dpos-3+i
        end
        cpos += 1
    end
    res = pos[1]
    println(cup[res+1], " ", cup[res+2])
end
game2("389125467",9,100)
```

    6 7
    

OK, the program reproduces part 1 correctly. Try carefully the full circle size


```julia
@elapsed game2("389125467",1000000,1000)
```

    7 10
    




    16.03783298



Even that is too slow, 1000 rounds in 16 seconds. I got rid of the linear search, but still have to move around possibly huge array slices and, in addition, update the `pos` array. Could try to move either everything up do destination down, or everything from destination to source up, whichever is shorter, but have little hope that this would change much


```julia
function game2(initconf,size,nround)
    cup1 = zeros(Int,size)    # which cup is at position
    pos = zeros(Int,size)    # which position has cup
    initarr = parse.(Int,split(initconf,""))
    for i in 1:length(initarr)
        cup1[i]=initarr[i]
        pos[initarr[i]] = i
    end
    for i in (length(initarr)+1):size
        cup1[i] = i
        pos[i] = i
    end
    cup = CircularArray{Int}(cup1)
    cpos = 1
    for n = 1:nround
        if count_ones(n) == 1
            println(n)
        end
        e3 = cup[(cpos+1):(cpos+3)]
        dpos = 0
        dcup = cup[cpos]
        while dpos == 0
            dcup = dcup-1
            if dcup < 1
                dcup = size
            end
            if !(dcup in e3)
                dpos = pos[dcup]
            end
        end
        # now everything from position cc+4 to dpos has to be shifted 3 positions left
        if mod((dpos-cpos-3),size)<size/2
            if dpos<cpos
                dpos += size
            end
            for i = (cpos+4):dpos
                cup[i-3] = cup[i]
                pos[cup[i]] = i-3
            end
        else
            if dpos>cpos
                dpos -= size
            end
            # when shifting elements up, have to go down with indices, not to overwrite destinations
            for i = (cpos+3):-1:(dpos+1)
                cup[i] = cup[i-3]
                pos[cup[i]] = i
            end
            cpos += 3
        end
        # and put the 3 cups after that
        for i = 1:3
            cup[dpos-3+i] = e3[i]
            pos[e3[i]] = dpos-3+i
        end
        cpos += 1
    end
    res = pos[1]
    println(cup[res+1], " ", cup[res+2])
end
game2("389125467",1000000,5)
```

    1
    2
    4
    999998 4
    


```julia
@elapsed game2("389125467",1000000,10000)
```

    1
    2
    4
    8
    16
    32
    64
    128
    256
    512
    1024
    2048
    4096
    8192
    999998 4
    




    2.634701429



A thousand times that is below 1 hour, but it will probably become slower, as the numbers become mixed, and the copies get longer... Need something even more completely different™. Maybe a form of a linked list? Each cup holds the position of the next cup in the circle. "Move next 3 cups to after destination" becomes: set next of current cup to next of 3 cups ahead, set that to the next on the destination and set next of the destination to the first cup moved. Just 3 copies, regardless of the distance between source and destination. Try it on the part 1 test case first.


```julia
function game3(initconf,size,nround)
    prevcup(x) = (x>1 ? x-1 : size)
    nxt = zeros(Int,size)
    initarr = parse.(Int,split(initconf,""))
    first = initarr[1]
    for i in 1:(length(initarr)-1)
        nxt[initarr[i]] = initarr[i+1]
    end
    if size == length(initarr)
        nxt[initarr[end]] = first
    else
        nxt[initarr[end]] = length(initarr)+1
        for i in (length(initarr)+1):(size-1)
            nxt[i] = i+1
        end
        nxt[size] = first
    end
    
    curr = first
    for i=1:nround
        e1 = nxt[curr]
        e2 = nxt[e1]
        e3 = nxt[e2]
        dest = prevcup(curr)
        while dest in [e1,e2,e3]
            dest = prevcup(dest)
        end
        nxt[curr] = nxt[e3]
        nxt[e3] = nxt[dest]
        nxt[dest] = e1
        curr = nxt[curr]
    end
    if size < 10
        println(nxt)
    end
    res1 = nxt[1]
    res2 = nxt[res1]
    println("r1 $res1 r2 $res2 result $(res1*res2)")
end
game3("389125467",9,100)
```

    [6, 9, 8, 5, 2, 7, 3, 4, 1]
    r1 6 r2 7 result 42
    


```julia
@elapsed game3("389125467",1000000,10000)
```

    r1 3 r2 4 result 12
    




    0.004110567



result a bit suspicious, expected it to me more thoroughly mixed after 10000 rounds, but well, time is good, so try to run the test to the end.


```julia
@elapsed game3("389125467",1000000,10000000)
```

    r1 934001 r2 159792 result 149245887792
    




    1.686315257



Fine, mixing is apparently not that fast as I expected. Run my data


```julia
game3("589174263",1000000,10000000)
```

    r1 137066 r2 21241 result 2911418906
    

I'm not proud of the way I handled this puzzle...
