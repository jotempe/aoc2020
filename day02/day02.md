# AoC 2020, day 2
Today, dear children, we are learning some basic string operations — at least from my point of view that's the main difficulty of today's puzzle.

Start with reading in the test and competition data sets as vectors of strings.


```julia
function readin(fname)
    f = open(fname)
    data = readlines(f)
    close(f)
    data
end
```




    readin (generic function with 1 method)




```julia
test = readin("test.txt")
input = readin("input.txt")
length(input)
```




    1000



Structure for a "database row". Julia, as I learn, has no concept of methods attached to objects. Objects are just structures and functions operate on them.


```julia
struct Pwd
    cmin :: Int
    cmax :: Int
    char :: Char
    pwd :: String
end
```

Now a function that parses an input line into the structure. Use of a regular expression seems appropriate.


```julia
function parsepwd(line)
    r = r"(\d+)-(\d+) (\w): (\w+)"
    m = match(r,line)
    m = m.captures
    n1 = parse(Int,m[1])
    n2 = parse(Int,m[2])
    c = m[3][1]
    Pwd(n1,n2,c,m[4])
end
parsepwd(test[1])
```




    Pwd(1, 3, 'a', "abcde")



Looks fine. There probably is a better way than doing those explicit conversion, but hey, I'm learning, OK?

So, now a function that validates a single password according to the policy.


```julia
function isvalid(line)
    p = parsepwd(line)
    n = count(c->(c==p.char),p.pwd)
    return n ≥ p.cmin && n ≤ p.cmax
end
isvalid.(test)
```




    3-element BitArray{1}:
     1
     0
     1



As a side note: in true R style Julia treats boolean `true` as integer 1, so this works:


```julia
sum(isvalid.(test))
```




    2



But a definitely cleaner way is to use the `count` function without a condition predicate, which does exactly what is needed, i.e. counts logical ones in the input vector.


```julia
count(isvalid.(input))
```




    655



## Part 2
All I need is to rewrite the `isvalid` function slightly. Note the `XOR` operator written as an Unicode character that three days ago I didn't even know existed.


```julia
function isvalid2(line)
    p = parsepwd(line)
    return (p.pwd[p.cmin]==p.char) ⊻ (p.pwd[p.cmax]==p.char)
end
```




    isvalid2 (generic function with 1 method)




```julia
isvalid2.(test)
```




    3-element BitArray{1}:
     1
     0
     0




```julia
count(isvalid2.(input))
```




    673



That was easy, once I got the regex matching and type conversions right. Upon reflection: the `Pwd` structure was not really needed, in the end it's been only used to internally transfer the result of parsing an input line to the validation function. I probably unconsciously expected, that part 2 will involve a more complicated manipulation of the input vector. 

And now, dear children, after solving the puzzle, let's discuss the strange case of a corporation that kept their password database unhashed, and a corporate salesman who exposed that entire database to a complete stranger. Must be some kind of the North Pole magic.
