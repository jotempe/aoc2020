# AoC 2020 day 1
As usual, day one problem is trivial. The main difficulty lies in the fact, that I have challenged myself to write in Julia, a language I barely know (well, who am I kidding, language I don't know).

So, for a starting problem, how to read in a table of numbers from a text file in Julia?


```julia
ft = open("test.txt")
test = parse.(Int,readlines(ft))
close(ft)
test
```




    6-element Array{Int64,1}:
     1721
      979
      366
      299
      675
     1456



Proud that I remembered what I have learned about that little dot after the function name.

OK, so now read the actual data and check the size to be sure.


```julia
f = open("input.txt")
data = parse.(Int,readlines(f)) 
close(f)
```


```julia
length(data)
```




    200



So the function that solves part 1 – nothing to comment here, really.


```julia
function part1(d)
    l = length(d)
    for i = 1:(l-1)
        for j = (i+1):l
            if d[i]+d[j] == 2020
                println("$i, $j, $(d[i]), $(d[j]), $(d[i]*d[j])")
                break
            end
        end
    end
end
```




    part1 (generic function with 1 method)




```julia
part1(test)
```

    1, 4, 1721, 299, 514579
    

Result as expected, so run on the real data


```julia
part1(data)
```

    67, 107, 437, 1583, 691771
    

## Part 2

Just add an inner loop, nothing interesting to look here, you may proceed.


```julia
function part2(d)
    l = length(d)
    for i = 1:(l-2)
        for j = (i+1):(l-1)
            for k = (j+1):l
                if d[i]+d[j]+d[k] == 2020
                    println("$i, $j, $k, $(d[i]), $(d[j]), $(d[k]), $(d[i]*d[j]*d[k])")
                    break
                end
            end
        end
    end
end
```




    part2 (generic function with 1 method)




```julia
part2(test)
```

    2, 3, 5, 979, 366, 675, 241861950
    


```julia
part2(data)
```

    28, 45, 177, 717, 335, 968, 232508760
    

Actually... I remember to have read that in Julia you could do multiple loops in one instruction. How did it go?


```julia
function part2a(d)
    l = length(d)
    for i = 1:(l-2), j = (i+1):(l-1), k = (j+1):l
        if d[i]+d[j]+d[k] == 2020
            println("$i, $j, $k, $(d[i]), $(d[j]), $(d[k]), $(d[i]*d[j]*d[k])")
            break
        end
    end
end
```




    part2a (generic function with 1 method)




```julia
part2a(data)
```

    28, 45, 177, 717, 335, 968, 232508760
    

Yup, works. This is actually better than the previous version, because `break` exits all loops in this setting, not just the innermost one.
