# AoC 2020 day 3
OK, 2-dim arrays are at work. 


```julia
f = open("test.txt")
a = split.(readlines(f),"")
close(f)
a
```




    11-element Array{Array{SubString{String},1},1}:
     [".", ".", "#", "#", ".", ".", ".", ".", ".", ".", "."]
     ["#", ".", ".", ".", "#", ".", ".", ".", "#", ".", "."]
     [".", "#", ".", ".", ".", ".", "#", ".", ".", "#", "."]
     [".", ".", "#", ".", "#", ".", ".", ".", "#", ".", "#"]
     [".", "#", ".", ".", ".", "#", "#", ".", ".", "#", "."]
     [".", ".", "#", ".", "#", "#", ".", ".", ".", ".", "."]
     [".", "#", ".", "#", ".", "#", ".", ".", ".", ".", "#"]
     [".", "#", ".", ".", ".", ".", ".", ".", ".", ".", "#"]
     ["#", ".", "#", "#", ".", ".", ".", "#", ".", ".", "."]
     ["#", ".", ".", ".", "#", "#", ".", ".", ".", ".", "#"]
     [".", "#", ".", ".", "#", ".", ".", ".", "#", ".", "#"]



What I actually got is an array of arrays, not a 2-dim array, no surprise here. `split` can not know in advance that all lines will have the same length. It will do, provided the length function works as expected.


```julia
length(a)
```




    11




```julia
length(a[1])
```




    11



Function `ntree` solves the first part. Make step length into a parameter of the function, because most probably we'll have to explore different steps in part 2.


```julia
function ntree(a,n)
    wd = length(a[1])
    sum = 0
    for i in 1:length(a)
        j = mod(n*(i-1),wd) + 1    # languages with 0-based arrays are better in this respect
        if a[i][j]=="#"
            sum += 1
        end
    end
    sum
end

ntree(a,3)
```




    7



Great, so do that for the real data


```julia
f = open("input.txt")
data = split.(readlines(f),"")
close(f)
ntree(data,3)
```




    169



# Part 2
Should have written the function with variable both x and y steps... Do it now. Don't bother changing the function name, take advantage of Julia's polymorphism.


```julia
function ntree(a,n,m)
    wd = length(a[1])
    sum = 0
    for i in 1:m:length(a)
        j = mod(n*(i-1)÷m,wd) + 1 
        if a[i][j]=="#"
            sum += 1
        end
    end
    sum
end
```




    ntree (generic function with 2 methods)




```julia
ntree(a,1,1)
```




    2




```julia
ntree(a,1,2)
```




    2



Function `res` solves the puzzle, write it as an one-liner for a change


```julia
res(x) = ntree(x,1)*ntree(x,3)*ntree(x,5)*ntree(x,7)*ntree(x,1,2)
```




    res (generic function with 1 method)




```julia
res(a)
```




    336



Test OK, we are ready for the solution.


```julia
res(data)
```




    7560370818


