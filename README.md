# README #

### What is this repository for? ###

My solutions to 2020 [Advent of code](https://adventofcode.com/) problems. 

The programming language is [Julia](https://julialang.org), in [Jupyter](https://jupyter.org/) notebooks environment. Each day is solved in
a separate notebook in a separate directory. In each day's solution directory you'll find:

+ the notebook itself (.ipynb file);
+ HTML rendering of the notebook (.html);
+ markdown rendering of the notebook (.md).

Apart from those, the daily directories often contain input data files. 

You may read the markdown version directly on BitBucket. The HTML versions look nicer, but you'll have to clone the repo (or at least
copy the HTML files to your computer) in order to read those. Of course if you install Jupyter and Julia, you'll be able to re-run my
calculations for yourself, e.g. on your input data. Have fun.