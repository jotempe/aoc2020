# AoC 2020 day 5

First part is just binary numbers written in a funny way. Trivially convert them to integers.


```julia
function seat(s)
    x = replace(s,'F' => '0')
    x = replace(x,'B' => '1')
    x = replace(x,'L' => '0')
    x = replace(x,'R' => '1')
    parse(Int,"0b"*x)
end

f = open("input.txt")
inp = readlines(f)
close(f)
seats = seat.(inp)
maximum(seats)
```




    880



## Part 2

If the number of seats were larger, then a cleverer algorithm that does not scan the seat list multiple times would be needed. But with this length of the list...


```julia
for i = 2:879
    if !(i in seats) && (i-1) in seats && (i+1) in seats
        println(i)
        break
    end
end
```

    731
    

And that was it? Seriously? Advent of Code is not what it used to be. Or maybe it is the year 2020 that is not what years used to be.

Just for kicks: how would I go about a list of ten million "seat numbers"? One strategy would be to sort the list and then scan it just once:


```julia
sseats = sort(seats)
for i = 1:length(sseats)-1
    if sseats[i+1]-sseats[i] == 2
        println(sseats[i]+1)
        break
    end
end
```

    731
    

In real life I would probably sort the list in place, to save some memory, but here I wanted to keep the original list intact.

Another viable strategy would be to build a list of seat occupation and then pick empty seats from it.


```julia
occ = zeros(Bool,880)
occ[seats] .= true
free = findall(.!occ)
```




    13-element Array{Int64,1}:
       1
       2
       3
       4
       5
       6
       7
       8
       9
      10
      11
      12
     731



That was a bit of R programming done in Julia: writing code that operates on arrays without explicit loops. All the Internet keeps telling me, that there is no reason to avoid loops in Julia, as they are fast. Moreover, the Julia runtime is written in Julia, so vector operations run similar loops to what I would have written myself. But it's still fun writing in this style.

## More
Still bored... maybe I could do better with `replace` in the data preparation phase? It is easy to change four replaces I used into two:


```julia
function seat2(s)
    x = replace(s, r"(F|L)" => "0")
    x = replace(x, r"(B|R)" => "1")
    parse(Int,"0b"*x)
end
seats2 = seat2.(inp)
seats == seats2
```




    true



But is it possible to do with just one replace? Hmmm, for collections it is possible to specify a series of old/new pairs, but apparently this multiple replacement does not work for strings:


```julia
replace(inp[1], "F"=>"0", "B"=>"1")
```


    MethodError: no method matching replace(::String, ::Pair{String,String}, ::Pair{String,String})
    Closest candidates are:
      replace(::AbstractString, ::Pair, ::Pair) at set.jl:592
      replace(::Any, ::Pair...; count) at set.jl:534
      replace(!Matched::Union{Function, Type}, ::Pair, ::Pair; count) at set.jl:591
      ...

    

    Stacktrace:

     [1] replace(::String, ::Pair{String,String}, ::Pair{String,String}) at .\set.jl:592

     [2] top-level scope at In[6]:1

     [3] include_string(::Function, ::Module, ::String, ::String) at .\loading.jl:1091


We can of course split the string into a collection of single character stings, do the replacements and join them back into a single string:


```julia
join(replace(split(inp[2],""), "F"=>"0", "B"=>"1", "L"=>"0", "R"=>"1"))
```




    "1010110101"



But this looks rather ugly, the "2 replaces" solution is still nicer. 

I do have, however, that nagging feeling that some clever replacement target could solve the problem in one step... Yep, no Julia manual I found mentions it, but the replacement may be a function, that gets the matched string as parameter, a'la Javascript. With use of an anonymous function it may look a bit confusing at first glance, but it works:


```julia
replace(inp[2], r"(F|L|B|R)" => x -> x=="F"||x=="L" ? "0" : "1")
```




    "1010110101"



Uff, at least I could learn something.
