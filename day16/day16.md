# AoC 2020 day 16
First part is an easy-looking testing if all given numbers are within given ranges. Julia has range as a data type, will use that. Reading the data in is more complicated than usual today.

The list of fields and ranges will be kept in a dictionary


```julia
function getdata(file)
    int(s) = parse(Int,s)
    ticket(s) = int.(split(s,","))    # ticket data is just a list of numbers separated by commas
    f = open(file)
    # Read fields and ranges
    fields = Dict()
    while true
        l = readline(f)
        if l==""
            break
        end
        m = match(r"([a-z ]+): (\d+)\-(\d+) or (\d+)\-(\d+)",l)
        fld = m.captures[1]
        r1 = int(m.captures[2]):int(m.captures[3])
        r2 = int(m.captures[4]):int(m.captures[5])
        fields[fld] = [r1,r2]
    end
    readline(f)           # Skip "your ticket:" line, read my ticket data
    mytic = ticket(readline(f))
    readline(f)          
    readline(f)           # Skip "other tickets:" and an empty line before it.
    tic = []              # this list will keep scanned tickets
    while !eof(f)
        push!(tic,ticket(readline(f)))
    end
    fields, mytic, tic
end
tf, tmy, toth = getdata("test.txt")
```




    (Dict{Any,Any}("class" => UnitRange{Int64}[1:3, 5:7],"row" => UnitRange{Int64}[6:11, 33:44],"seat" => UnitRange{Int64}[13:40, 45:50]), [7, 1, 14], Any[[7, 3, 47], [40, 4, 50], [55, 2, 20], [38, 6, 12]])



Looks OK. Now, for each number in each ticket in the list check if is in any of the valid ranges. Maybe I should have constructed a union of all ranges instead of checking numbers against them individually? OK, for the size of data at hand it makes no real difference, but it is something worth remembering and using, should speed up the checking considerably. 


```julia
function sol1(flds,tics)
    s = 0                    # This will keep the sum of incorrect numbers
    for t in tics, n in t    # Those multiple loops are surprisingly handy
        hit = false
        for r in values(flds)
            if n in r[1] || n in r[2]
                hit = true
                break
            end
        end
        if !hit
            s = s+n
        end
    end
    s
end
sol1(tf,toth)            
```




    71




```julia
inf, inmy, inoth = getdata("input.txt")
sol1(inf,inoth)
```




    32835



## Part 2
OK, that was predictable. First a function that discards bad tickets. Note that we have a different test data set for part 2. Also we unfortunately need an array, not a dictionary, for the fields, because we have to preserve ordering. What I was thinking when deciding to use a dictionary? Must be that early hour... So a new `getdata` is needed, similar to the original one, that would read the field data into an array of `Field` structures


```julia
struct Field
    name ::String
    r1
    r2
end
function getdata2(file)
    int(s) = parse(Int,s)                # shorthand for int2str()
    ticket(s) = int.(split(s,","))       # ticket data is just a list of numbers separated by commas
    f = open(file)
    # Read fields and ranges
    fields = []
    while true
        l = readline(f)
        if l==""
            break
        end
        m = match(r"([a-z ]+): (\d+)\-(\d+) or (\d+)\-(\d+)",l)
        fld = m.captures[1]
        r1 = int(m.captures[2]):int(m.captures[3])
        r2 = int(m.captures[4]):int(m.captures[5])
        push!(fields,Field(fld,r1,r2))
    end
    readline(f)
    mytic = ticket(readline(f))
    readline(f)
    readline(f)
    tic = []
    while !eof(f)
        push!(tic,ticket(readline(f)))
    end
    fields, mytic, tic
end
```




    getdata2 (generic function with 1 method)




```julia
tf2, tmy2, toth2 = getdata2("test2.txt")
```




    (Any[Field("class", 0:1, 4:19), Field("row", 0:5, 8:19), Field("seat", 0:13, 16:19)], [11, 12, 13], Any[[3, 9, 18], [15, 1, 5], [5, 14, 9]])



The `discard` function below discards bad tickets from the set. There are none in the second test set, so no need to run it on that set.


```julia
function discard(flds,tics)
    out = []
    for t in tics
        good = true
        for n in t 
            hit = false
            for f in flds
                if n in f.r1 || n in f.r2
                    hit = true
                    break
                end
            end
            good = good && hit
            if !good
                break
            end
        end
        if good
            push!(out,t)
        end
    end
    out
end
inf, inmy, inoth = getdata2("input.txt")
println(length(inoth))
inoth = discard(inf,inoth)
length(inoth)
```

    244
    




    190



54 bad tickets... OK. Now what? Construct for each of the fields on a ticket a list of valid field names. Run through all the tickets and remove field name from the corresponding list of valid fields if a value falls off the valid ranges for the field. Use sets to represent still valid field names for each position. Start with each position assigned a full set of known field names, and delete those excluded as you go through the ticket list. 


```julia
function valid(flds,tics)
    fnames = [f.name for f in flds]
    fs = Set(fnames)
    vnames = [copy(fs) for i in axes(tics[1],1)]
    for t in tics
        for i in axes(t,1)
            for f in flds
                if !(t[i] in f.r1 || t[i] in f.r2)
                    delete!(vnames[i],f.name)
                end
            end
        end
    end
    vnames
end
valid(tf2,toth2)
```




    3-element Array{Set{String},1}:
     Set(["row"])
     Set(["class", "row"])
     Set(["class", "row", "seat"])



OK, so it looks that we'll have to do something similar to [Aoc 2018 day 16](https://adventofcode.com/2018/day/16). For many fields we have multiple "candidate" names, and we have to use elimination to find the (hopefully) unique assignment. So get the list of valid field names for the competition input.

You may have noticed, BTW, that I'm now obviously skewed towards using the `axes()` function instead of more intuitive `1:length()`. Hope it will eventually go away.


```julia
val = valid(inf,inoth)
```




    20-element Array{Set{String},1}:
     Set(["departure date", "route", "departure location", "class", "arrival station", "wagon", "row", "departure platform", "duration", "arrival platform", "departure station", "arrival location", "train", "departure track", "arrival track", "seat", "type", "departure time"])
     Set(["route", "class", "departure platform", "duration", "arrival location", "train", "arrival track", "seat", "type", "departure time"])
     Set(["route", "class", "departure platform", "duration", "departure station", "arrival location", "train", "departure track", "arrival track", "seat", "type", "departure time"])
     Set(["route", "class", "duration", "arrival location", "train", "arrival track", "seat"])
     Set(["class", "duration", "arrival location", "train", "arrival track", "seat"])
     Set(["departure date", "route", "departure location", "class", "arrival station", "row", "departure platform", "duration", "arrival platform", "departure station", "arrival location", "train", "departure track", "arrival track", "seat", "type", "departure time"])
     Set(["route", "class", "departure platform", "duration", "arrival location", "train", "arrival track", "seat", "type"])
     Set(["class", "arrival location", "arrival track"])
     Set(["departure date", "zone", "route", "departure location", "class", "arrival station", "wagon", "row", "departure platform", "duration", "arrival platform", "departure station", "arrival location", "train", "departure track", "arrival track", "seat", "type", "departure time"])
     Set(["class", "arrival location"])
     Set(["class", "duration", "arrival location", "train", "arrival track"])
     Set(["class", "arrival location", "train", "arrival track"])
     Set(["class"])
     Set(["route", "class", "departure platform", "duration", "arrival location", "train", "departure track", "arrival track", "seat", "type", "departure time"])
     Set(["departure date", "route", "class", "departure platform", "duration", "departure station", "arrival location", "train", "departure track", "arrival track", "seat", "type", "departure time"])
     Set(["departure date", "route", "departure location", "class", "departure platform", "duration", "departure station", "arrival location", "train", "departure track", "arrival track", "seat", "type", "departure time"])
     Set(["route", "class", "duration", "arrival location", "train", "arrival track", "seat", "type"])
     Set(["departure date", "route", "departure location", "class", "arrival station", "row", "departure platform", "duration", "departure station", "arrival location", "train", "departure track", "arrival track", "seat", "type", "departure time"])
     Set(["departure date", "route", "departure location", "class", "arrival station", "departure platform", "duration", "departure station", "arrival location", "train", "departure track", "arrival track", "seat", "type", "departure time"])
     Set(["departure date", "zone", "price", "route", "departure location", "class", "arrival station", "wagon", "row", "departure platform", "duration", "arrival platform", "departure station", "arrival location", "train", "departure track", "arrival track", "seat", "type", "departure time"])



Use the following function to "cross out" names that already have an unique name assigned. The style of that function is rather poor, so some explanations. We have a "known" array that marks fields whose names are unique and have already been crossed out from other candidate name lists. Search the list for first field with only a single element in the valid names list, mark it as known and cross out that name from all other list. Note that the loop in line 10 (`for elem in val[i]`) will by definition run only once. But I found no other way to extract the single element from a 1-element set, because sets are unordered collections and indexing is not possible. OK, I found a rather clumsy alternative `elem = [f for f in val[i]][1]`, i.e. change set into an array and pick the first element, but a for loop is probably better than that. Especially that this latter expression also contains an implicit loop.


```julia
function clean(val)
    known = zeros(Bool,length(val))
    changed = true
    while changed
        changed = false
        for i in axes(val,1)
            if !known[i] && length(val[i]) == 1
                changed = true
                known[i] = true
                for elem in val[i]
                    for j in axes(val,1)
                        if !known[j]
                            delete!(val[j],elem)
                        end
                    end
                end
            end
        end
    end
    val
end
clean(val)
```




    20-element Array{Set{String},1}:
     Set(["wagon"])
     Set(["departure time"])
     Set(["departure station"])
     Set(["route"])
     Set(["seat"])
     Set(["arrival platform"])
     Set(["departure platform"])
     Set(["arrival track"])
     Set(["zone"])
     Set(["arrival location"])
     Set(["duration"])
     Set(["train"])
     Set(["class"])
     Set(["departure track"])
     Set(["departure date"])
     Set(["departure location"])
     Set(["type"])
     Set(["row"])
     Set(["arrival station"])
     Set(["price"])



Great! Now in principle I should write a program to pick the indices of desired fields, but I should be at work already, so just count and write the expression down by hand.


```julia
inmy[2]*inmy[3]*inmy[7]*inmy[14]*inmy[15]*inmy[16]
```




    514662805187



I'm back. Now that we have the answer and the star, how to compute it "correctly"?


```julia
ilo = 1
for i in axes(val,1)
    for elem in val[i]
        if occursin("departure",elem)
            ilo = ilo*inmy[i]
        end
    end
end
ilo
```




    514662805187



Hm, I don't like it, because of looping over explicit indices. Wonder if Julia has an analog of Python's `zip()` function to create a collection of pairs (or tuples) from corresponding elements of two or more arrays. Yes, it has, and it is called (surprise) `zip()`. So, a nicer version of the above is:


```julia
ilo = 1
for (s,num) in zip(val,inmy)
    for elem in s
        if occursin("departure",elem)
            ilo = ilo*num
        end
    end
end
ilo
```




    514662805187



The `clean` function could be rewritten in a nicer form using `zip` as well. This is left as an exercise to the reader. 

BTW: here is our ticket printed nicely using `hcat` function, which is similar to `zip`, but it produces a 2-dim array from two vectors, not a collection of pairs.


```julia
hcat([[s for s in f][1] for f in val],inmy)
```




    20×2 Array{Any,2}:
     "wagon"                89
     "departure time"      193
     "departure station"    59
     "route"               179
     "seat"                191
     "arrival platform"    173
     "departure platform"   61
     "arrival track"        73
     "zone"                181
     "arrival location"     67
     "duration"             71
     "train"               109
     "class"                53
     "departure track"      79
     "departure date"       83
     "departure location"  113
     "type"                107
     "row"                 139
     "arrival station"     131
     "price"               137



What, class 53? C'mon, I can afford class 48!

## More
Some fun with range unions in Julia, to check that my comment from part 1 solution is indeed valid


```julia
rng = inf[1].r1
for f in inf
    rng = rng ∪ f.r1 ∪ f.r2
end
rng
```




    950-element Array{Int64,1}:
      30
      31
      32
      33
      34
      35
      36
      37
      38
      39
      40
      41
      42
       ⋮
     967
     968
     969
     970
     971
     972
     973
     974
      25
      26
      27
      28



OK, so union of disjoint ranges is just represented as a list of valid values. Nice, but treacherous when working with long ranges. So, checking against union of ranges instead of each range individually it not always a good idea. BTW, what happens if ranges overlap?


```julia
(1:4) ∪ (2:7)
```




    7-element Array{Int64,1}:
     1
     2
     3
     4
     5
     6
     7



Apparently it always changes ranges into list when doing union. OK, well, accept it as a life fact. Intersection of ranges, on the other hand, produces ranges:


```julia
(1:5) ∩ (2:8)
```




    2:5




```julia
(1:4) ∩ (5:7)
```




    5:4



The last one is not what I expected! Or rather in a way it is, because a range with upper limit higher than lower limit is actually empty, unless a negative step is explicitly given. We can check that:


```julia
for i in 5:1
    println(i)
end
println("end")
```

    end
    

So yes, it is correct, but strange. So, if we are at that how do intersections work with ranges that have step?


```julia
(1:2:5) ∩ (2:2:7)
```




    1:2:0



Right, the first range selects only odd numbers, the second only even numbers, so the intersection is empty.


```julia
(0:3:100) ∩ (0:5:100)
```




    0:15:90



Nice. The upper limit has been adjusted to the actually highest number in range


```julia
(1:5) ∩ (5:-1:1)
```




    1:1:5




```julia
(5:-1:1) ∩ (1:5)
```




    5:-1:1



So the direction seems inherited from the first range.
