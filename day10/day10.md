# AoC 2020 day 10
Sounds trivial, just sort the input list and count the differences between neighbouring elements. No point to even run tests...


```julia
function getlist(file)
    f=open(file)
    l=parse.(Int,readlines(f))
    close(f)
    l
end

sl = getlist("input.txt")
push!(sl,0)     # Add the "source" rated 0 to the list
sort!(sl)
nd1 = 0
nd3 = 1         # The last difference is always 3, can count it already instead of adding to the list
for i = 1:length(sl)-1
    d = sl[i+1]-sl[i]
    if d == 1
        nd1+=1
    elseif d == 3
        nd3+=1
    end
end
println("$nd1, $nd3, $(nd1*nd3)")
```

    65, 26, 1690
    

## Part 2
Hmmm, let's think. We could build the solution incrementally. Start with the input list sorted by rating. Now: if the list contains an adapter rated 1 lower, then we have a valid sequence, same if it contains an adapter rated 2 lower and same if it contains one rated 3 lower. Thus the number of combinations up to this rating is the sum of the numbers of combinations for those up to three adapters. Will need some code to bootstrap the list, so that we can safely examine the adapter at current index-3, without producing index error.

Note that we don't need to add the rating of the device itself to the list, as it is always rated 3 higher than the last adapter, so the final number of combinations will be the same as the number of combinations for the last adapter.


```julia
function ncomb(adapters)
    l = sort(adapters)
    n = length(l)
    nc = zeros(Int,n)
    # assume the data are correct, so that difference between neighbour adapters is always > 0 and ≤ 3
    nc[1] = 1      # this is the source rate 0, only one exists
    nc[2] = 1      # lowest rated adapter has 1 combination, can be only connected to the source
    if l[3]-l[1] <= 3   # the next one may have 2 or 1 combinations, depending of its rating is 3 or more
        nc[3] = nc[1] + nc[2]
    else
        nc[3] = nc[2]
    end
    for i in 4:n
        nc[i] = nc[i-1]   # you may always connect it after the previous one
        if l[i]-l[i-2] <= 3  # check if you can connect it after one before that
            nc[i] += nc[i-2]   
            if l[i]-l[i-3] <= 3  # and maybe you can even connect it after 2 before previous
                nc[i] += nc[i-3]
            end
        end
    end
    nc[n]
end

st = getlist("test.txt")
push!(st,0)
ncomb(st)
```




    19208



Test OK, solve the problem.


```julia
ncomb(sl)
```




    5289227976704



At last, a puzzle that required a little bit more than brute force. OK, [day 7](https://adventofcode.com/2020/day/7) also required a bit of thinking, but more about data structures than algorithms. 

BTW: here's another interesting idea for a solution: note that whenever the sorted list of adapters has gap of 3 between consecutive ones, they both must be included in any valid combination (we know from part one, that there 26 such gaps). Thus we could split the entire list into 27 short sublists separated by gaps of 3, calculate the number of valid combinations for each of those sublists, and the answer would be the product of all those numbers. 

One nagging question I have: how did our hero fit all those adapters into their bag, if the bag is already filled with almost 20k other bags? 😁

## More
Thinking about the idea of counting combinations in small isolated groups and multiplying them together, I tried to factorise the results I got and... look for yourself:


```julia
using Primes
factor(ncomb(st))
```




    2^3 * 7^4




```julia
factor(ncomb(sl))
```




    2^17 * 7^9



Noticed it? Only factors of 2 and 7. Does it have to be so? No, a simplest counterexample is gap of 2 followed by two gaps of 1: you may use all adapters or leave out one in the middle, but not both. That makes 3 combinations.


```julia
sx=[0,2,3,4]
ncomb(sx)
```




    3



Factor 7 appears when e.g. you have 5 adapters with gaps of 1 between them. You may then use any combination of the three "middle" ones, except for an empty combination, which makes 8-1=7 possible arrangements.


```julia
sx=[0,1,2,3,4]
ncomb(sx)
```




    7



Higher factors are possible with longer gap-1 sequences:


```julia
sx=[0,1,2,3,4,5]
ncomb(sx)
```




    13




```julia
sx=[i for i=0:6]
factor(ncomb(sx))
```




    2^3 * 3




```julia
sx=[i for i=0:7]
factor(ncomb(sx))
```




    2^2 * 11




```julia
sx=[i for i=0:8]
factor(ncomb(sx))
```




    3^4




```julia
sx=[i for i=0:9]
factor(ncomb(sx))
```




    149



Adding gaps of 2 you may get other factors as well: for example a group of 3 gap-1 adapters surrounded by gap 2 on both sides gives 5 possible combinations: you may connect all of them, you may throw away either or both first/last adapter from the gap-1 group and you may remove the middle adapter, but then no other one can be removed. 


```julia
sx=[0,2,3,4,6]
ncomb(sx)
```




    5



So, the fact that both test and competition data sets produce only factors 2 and 7 in the resultant count of combinations is either a coincidence, or result of the way puzzle author prepared the input data. 
