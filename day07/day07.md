# AoC 2020 day 7
Looks like again we'll be exercising regular expressions to read the stuff in. Plus, maybe a dictionary data structure would be useful? Container colour as key and a list of number-colour pairs as value. And, for the first part of the puzzle, a second dictionary, that for each colour contains a list of colours that directly contain it. 

Let's name the first dictionary `down` (because it drills down into bag contents) and the other one `up`. We can construct them in parallel, as we read the data.


```julia
function readdata(file)
    f = open(file)
    down = Dict()    # Julia has several dictionary types, dunno which will be optimal in this case, so use default
    up = Dict()
    for line in eachline(f)
        # first break the line into container descriptions and content
        m = match(r"([a-z ]+) bags contain (.+)\.$",line)
        if m === nothing
            println("$line not matched")
            continue
        end
        bag = m.captures[1]
        content = m.captures[2]
        if occursin(r"^no",content)
            # if the content list starts with "no", the bag is a leaf node in the graph, contains no other bags
            down[bag] = []   # empty list is probably better than "nothing", because we can still iterate over it
        else
            list = []
            for c in split(content, ", ")
                m = match(r"(\d+) (.+) bag",c)
                if m === nothing
                    println("$c not matched 2")
                    continue
                end
                cnt = parse(Int, m.captures[1])
                col = m.captures[2]
                push!(list, (cnt, col))
                # see if entry in the "up" dictionary already exists and either create or update it
                if haskey(up,col)
                    push!(up[col],bag)
                else
                    up[col] = [bag]
                end
            end
            down[bag] = list
        end
    end
    close(f)
    (up,down)   # return a pair of dictionaries
end
testup, testdown = readdata("test.txt")
# Print both test dictionaries out, to check them
for i in testup
    println(i)
end
println()
for i in testdown
    println(i)
end
```

    Pair{Any,Any}("muted yellow", SubString{String}["light red", "dark orange"])
    Pair{Any,Any}("shiny gold", SubString{String}["bright white", "muted yellow"])
    Pair{Any,Any}("dotted black", SubString{String}["dark olive", "vibrant plum"])
    Pair{Any,Any}("vibrant plum", SubString{String}["shiny gold"])
    Pair{Any,Any}("faded blue", SubString{String}["muted yellow", "dark olive", "vibrant plum"])
    Pair{Any,Any}("dark olive", SubString{String}["shiny gold"])
    Pair{Any,Any}("bright white", SubString{String}["light red", "dark orange"])
    
    Pair{Any,Any}("muted yellow", Any[(2, "shiny gold"), (9, "faded blue")])
    Pair{Any,Any}("shiny gold", Any[(1, "dark olive"), (2, "vibrant plum")])
    Pair{Any,Any}("dotted black", Any[])
    Pair{Any,Any}("dark orange", Any[(3, "bright white"), (4, "muted yellow")])
    Pair{Any,Any}("vibrant plum", Any[(5, "faded blue"), (6, "dotted black")])
    Pair{Any,Any}("faded blue", Any[])
    Pair{Any,Any}("dark olive", Any[(3, "faded blue"), (4, "dotted black")])
    Pair{Any,Any}("light red", Any[(1, "bright white"), (2, "muted yellow")])
    Pair{Any,Any}("bright white", Any[(1, "shiny gold")])
    

Makes sense. So try to answer the first part using the `up` dictionary. Assume there are no loops in the graph.


```julia
function listup(bag,dict)
    if !haskey(dict,bag)
        return []
    else
        l = copy(dict[bag])      # we don't want to modify the dictionary!
        for b in dict[bag]
            append!(l, listup(b,dict))
        end
        return l
    end
end
listup("shiny gold",testup)
```




    6-element Array{SubString{String},1}:
     "bright white"
     "muted yellow"
     "light red"
     "dark orange"
     "light red"
     "dark orange"



There are repetitions in the list, because our bag can be either in `bright white` or `muted yellow`, and these both have same "parents" (bags they could be in). Count the list after removing those.


```julia
length(unique(listup("shiny gold",testup)))
```




    4



And for the data


```julia
inup, indown=readdata("input.txt")
length(unique(listup("shiny gold",inup)))
```




    348



## Part 2
Hey, my guess was right. We'll need the "down" dictionaries now, that we have already constructed. Again, use recursion, this time to drill into bag content.


```julia
function countin(bag,dict)
    n = 0
    for b in dict[bag]               # Remember, b is a pair (count, colour) now
        n += b[1]*countin(b[2],dict)
    end
    return n+1 # remember to count "yourself"
end
countin("shiny gold",testdown)-1  # subtract the outmost shiny gold bag 
```




    32



Test OK, real run:


```julia
countin("shiny gold",indown)-1
```




    18885



But it seems to me that my solution is rather ugly, I did not get the spirit of the language yet...
