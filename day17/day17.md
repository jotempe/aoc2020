# AoC 2020 day 17
A 3-d version of the [Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life). With rules identical to the original Conway's game of life rules.

The first question is: what data structure is best to solve the problem? We could try a 3-d matrix, however the the grid is supposed to be "infinite". It is possible to predict the maximal size, but what if we are asked about some huge number of moves in part 2? In principle we could probably "grow" the matrix dynamically in Julia, but it may cost a lot of memory and mean lots of copying the arrays around. We could use a dictionary indexed by triples of numbers, with elements added dynamically as they are needed. We'll need two copies of it during the "transition", as we have to keep the current state unchanged when building the new one. Finally we could keep the current state simply as a list of non-empty nodes. Good question how the `in` operator works in Julia, hope it is not linear search.

So, let's go with the last idea. We keep a list of non-empty grid points and at each step look at the neighbourhood of existing points to identify possible newly activated nodes (they must be next to some existing active node from previous step).


```julia
struct Cube
    x ::Int
    y ::Int
    z ::Int
end
function readinp(file)
    f = open(file)
    state = []
    lines = readlines(f)
    for i=1:length(lines), j=1:length(lines[i])
        if lines[i][j] == '#'
            push!(state,Cube(i,j,0))
        end
    end
    close(f)
    state
end

function countn(p::Cube, state)
    # counts cubes in a 3x3x3 grid around it, including it!
    cnt = 0
    for x in (p.x-1):(p.x+1), y=(p.y-1):(p.y+1), z=(p.z-1):(p.z+1)
        if Cube(x,y,z) in state
            cnt += 1
        end
    end
    cnt
end

function step(state)
    news = []
    for c in state
        # first determine if the cube survives.
        if countn(c, state) in 3:4
            push!(news, c)
        end
        # now search its neighbourhood for inactive cubes and activate those, that should be active, but are not yet.
        for x in (c.x-1):(c.x+1), y=(c.y-1):(c.y+1), z=(c.z-1):(c.z+1)
            nc = Cube(x,y,z)
            if nc in state || nc in news
                continue
            else
                if countn(nc,state)==3
                    push!(news,nc)
                end
            end
        end
    end
    news
end

state = readinp("test.txt")
for i in 1:6
    state = step(state)
end
length(state)
```




    112



Btw, the input test shape is called [glider](https://en.wikipedia.org/wiki/Glider_(Conway%27s_Life)), and in standard 2-d game of Life is a self-replicating shape moving across the board. As we can see, in 3d it is nothing like that. 

Let's run the competition data set. 


```julia
state = readinp("input.txt")
for i in 1:6
    state = step(state)
end
length(state)
```




    346



## Part 2
OK... add one dimension and go.


```julia
struct Hcube
    x ::Int
    y ::Int
    z ::Int
    w ::Int
end
function readinp(file)
    f = open(file)
    state = []
    lines = readlines(f)
    for i=1:length(lines), j=1:length(lines[i])
        if lines[i][j] == '#'
            push!(state,Hcube(i,j,0,0))
        end
    end
    close(f)
    state
end

function countn(p::Hcube, state)
    # counts cubes in a 3x3x3 grid around it, including it!
    cnt = 0
    for x in (p.x-1):(p.x+1), y=(p.y-1):(p.y+1), z=(p.z-1):(p.z+1), w=(p.w-1):(p.w+1)
        if Hcube(x,y,z,w) in state
            cnt += 1
        end
    end
    cnt
end

function step(state)
    news = []
    for c in state
        # first determine if the cube survives.
        if countn(c, state) in 3:4
            push!(news, c)
        end
        # now search its neighbourhood for inactive cubes and activate those, that should be active, but are not yet.
        for x=(c.x-1):(c.x+1), y=(c.y-1):(c.y+1), z=(c.z-1):(c.z+1), w=(c.w-1):(c.w+1)
            nc = Hcube(x,y,z,w)
            if nc in state || nc in news
                continue
            else
                if countn(nc,state)==3
                    push!(news,nc)
                end
            end
        end
    end
    news
end

state = readinp("test.txt")
for i in 1:6
    state = step(state)
end
length(state)
```




    848



Hmmm... Too slow. Probably the Julia `in` operation searches arrays linearly. Go back to the idea of using dictionaries. Or... sets? They are unordered, hopefully the `in` operator searches them by hash. So, board state is a set of active cube coordinates.


```julia
function readinp(file)
    f = open(file)
    state = Set()
    lines = readlines(f)
    for i=1:length(lines), j=1:length(lines[i])
        if lines[i][j] == '#'
            push!(state,Hcube(i,j,0,0))
        end
    end
    close(f)
    state
end


function countn(c::Hcube,state)
    count(c->c ∈ state,
        [Hcube(i,j,k,l) for i=(c.x-1):(c.x+1), j=(c.y-1):(c.y+1), k=(c.z-1):(c.z+1), l=(c.w-1):(c.w+1)])
end

function step2(state)
    news = Set()
    for c in state
        # first determine if the cube survives.
        if countn(c, state) in 3:4
            push!(news, c)
        end
        # now search its neighbourhood for inactive cubes and activate those, that should be active, but are not yet.
        for x in (c.x-1):(c.x+1), y=(c.y-1):(c.y+1), z=(c.z-1):(c.z+1), w=(c.w-1):(c.w+1)
            nc = Hcube(x,y,z,w)
            if nc ∈ state || nc ∈ news
                continue
            else
                if countn(nc,state)==3
                    push!(news,nc)
                end
            end
        end
    end
    news
end

state = readinp("test.txt")
for i in 1:6
    state = step2(state)
end
length(state)
```




    848



The difference is really huge. Now the time is reasonable. Why, oh why, after using sets in several previous problems, did I abandon them? Must be that early hour again. 


```julia
state = readinp("input.txt")
for i in 1:6
    state = step2(state)
end
length(state)
```




    1632


