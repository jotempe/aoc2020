# AoC 2020 day 21
Looks like work with sets again. If I understand the problem correctly, I have to identify which of the ingredients contains which allergen, by identifying an ingredient present in all the foods containing given allergen. Having that identified uniquely, the given allergen and ingredient can be crossed out from the corresponding lists. Then I'll be left with ambiguous cases (there is one in the example list) where an allergen can be in a list of ingredients and I can't determine which. So I have to cross out all of them. 


```julia
function readin(file)
    lines = readlines(file)
    foods = []
    for l in lines
        m = match(r"(.+) \(contains (.+)\)",l)
        ingr = Set(split(m.captures[1]," "))
        allrg = Set(split(m.captures[2],", "))
        push!(foods,(ingr,allrg))
    end
    foods
end
tfoods = readin("test.txt")
```




    4-element Array{Any,1}:
     (Set(SubString{String}["sqjhc", "mxmxvkd", "kfcds", "nhms"]), Set(SubString{String}["fish", "dairy"]))
     (Set(SubString{String}["sbzzf", "trh", "fvjkl", "mxmxvkd"]), Set(SubString{String}["dairy"]))
     (Set(SubString{String}["sqjhc", "fvjkl"]), Set(SubString{String}["soy"]))
     (Set(SubString{String}["sbzzf", "sqjhc", "mxmxvkd"]), Set(SubString{String}["fish"]))



Now: find an allergen that occurs in more than one food. Intersect all ingredients lists for foods that contain it. If the intersection has exactly one element, we have discovered ingredient with allergen. Cross the allergen and the ingredient from all lists. Repeat until the list unchanged, remaining allergens are ambiguous.

On the test data set we'll be left with one allergen that is "ambiguous", may be in two ingredients. Cross those out either from the big list, and we should be left with a list of "safe" ingredients.


```julia
function safeingr(infoods)
    # Operate on a copy of input: sets are mutable, we don't want do destroy the original data
    foods = deepcopy(infoods)
    # Produce sets of all known igredients and all known allergens. Sumehow can't get union to work on a collection of sets)
    allingr = Set()
    allalrg = Set()
    for f in foods
        allingr = allingr ∪ f[1]
        allalrg = allalrg ∪ f[2]
    end
    known = []
    change = true
    while change
        change = false
        for a in allalrg
            # Get a list of foods containing this allergen and produce 'candidate ingredient' set
            fw = filter(f->a in f[2], foods)
            if length(fw)>1
                candingr = copy(fw[1][1])
                for f in fw
                    candingr = candingr ∩ f[1]
                end
                if length(candingr)==1
                    # We have a unique ingredient->allergen pair
                    for c in candingr
                        push!(known,(c,a))
                        # remove ingredient from allingr list and all individual ingredient lists
                        delete!(allingr,c)
                        delete!(allalrg,a)
                        for f in foods
                            delete!(f[1],c)
                            delete!(f[2],a)
                        end
                    end
                    change = true
                end
            end
        end
    end
    # remove from remaining ingredient set those ingredients, that exst in foods still containing unassigned allergens
    for f in foods
        if length(f[2])>0
            for i in f[1]
                delete!(allingr,i)
            end
        end
    end
    # The last step: count how many times remaining safe ingredients occurr in foods
    cnt = 0
    for i in allingr
        cnt += count(f->i in f[1],foods)
    end
    println("counted $cnt")
    known,foods
end
tknown, tredfoods = safeingr(tfoods)
```

    counted 5




    (Any[("mxmxvkd", "dairy"), ("sqjhc", "fish")], Any[(Set(SubString{String}["kfcds", "nhms"]), Set{SubString{String}}()), (Set(SubString{String}["sbzzf", "trh", "fvjkl"]), Set{SubString{String}}()), (Set(SubString{String}["fvjkl"]), Set(SubString{String}["soy"])), (Set(SubString{String}["sbzzf"]), Set{SubString{String}}())])



    
    


```julia
ifoods = readin("input.txt")
iknown, iredfoods = safeingr(ifoods)
iknown
```

    counted 2436
    




    8-element Array{Any,1}:
     ("dhfng", "dairy")
     ("znrzgs", "wheat")
     ("nqbnmzx", "shellfish")
     ("ntggc", "soy")
     ("pgblcd", "eggs")
     ("xhkdc", "fish")
     ("dstct", "sesame")
     ("ghlzj", "peanuts")



## Part 2
From the description of the problem I understand, that all ingredients with allergens should be identifiable now, by the fact, that there exist some food with that allergen that contains no other ingredients. So, iterate removals of ingredients and allergens over foods with one each.

Good that I returned the "reduced food list" from the `safeingr` function in part 1, it is our starting point now.


```julia
function checkremain(rfoods,known)
    # Add to the known ingredients with allergens list those, that 
    change = true
    while change
        change = false
        for f in rfoods
            if length(f[1]) == 1 && length(f[2]) == 1
                for i in f[1]
                    for a in f[2]
                        push!(known,(i,a))
                        for f1 in rfoods
                            delete!(f1[1],i)
                            delete!(f1[2],a)
                        end
                    end
                end
                change = true
            end
        end
    end
    known
end
ttknown = copy(tknown)
ttredfoods = deepcopy(tredfoods)
ttknown = checkremain(ttredfoods,ttknown)
```




    3-element Array{Any,1}:
     ("mxmxvkd", "dairy")
     ("sqjhc", "fish")
     ("fvjkl", "soy")



Great, try it on the production dataset.


```julia
iknown = checkremain(iredfoods,iknown)
```




    8-element Array{Any,1}:
     ("dhfng", "dairy")
     ("znrzgs", "wheat")
     ("nqbnmzx", "shellfish")
     ("ntggc", "soy")
     ("pgblcd", "eggs")
     ("xhkdc", "fish")
     ("dstct", "sesame")
     ("ghlzj", "peanuts")



No change? Probably my food list after part 1 does not contain any food with allergens. Check that.


```julia
for f in iredfoods
    if length(f[2])>0
        println(f)
    end
end
```

None indeed. If I checked that before, I could have spared myself writing the `checkremain` function. Wait, there should be nothing remaining after `safeingr` because it should have identified everything. Why did it not identify the ingredient with soy? A, I see, the flaw in line 18 of that function: I require that the list of foods with that allergen is longer than 1. The condition is unnecessary, if it is 1 i should have taken just the list of ingredients of that one and only food. 

Anyhow, what is left is to learn how to sort array of pairs by the second element of the pair in Julia. No surprise here, use a lambda to construct the sort key and pass it as `by` parameter. The `println` of the final string is there because this way I get the output without double quotes, so it is easier to copy it and paste into the AoC page.


```julia
sknown = sort(iknown, by=x->x[2])
println(join([x[1] for x in sknown],","))
```

    dhfng,pgblcd,xhkdc,ghlzj,dstct,nqbnmzx,ntggc,znrzgs
    

And that was it? On day 21 of AoC? Unbelievable. 
