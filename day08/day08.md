# AoC 2020 day 8
Aha! A processor again. Traditionally AoC in past years used such "processors" in several later puzzles, usually making them a bit more complicated each time. 

First, how to best implement it in Julia? In Python I used to implement it as a class, in Julia we have no classes, what we need is a data structure for processor state and a function to execute its program, that operates on that structure. 


```julia
struct Memcell    # This represents a single processor instruction
    instr::String
    offset::Int
end

mutable struct Processor
    acc::Int
    pc::Int     # This is the program counter, indicating the next instruction to execute
    mem::Array{Memcell}
end

function execinstr(p::Processor)
    # executes a single instruction in processor state p.
    c = p.mem[p.pc]    # This is the instruction to be executed
    if c.instr == "nop"
        p.pc += 1
    elseif c.instr == "acc"
        p.acc += c.offset
        p.pc += 1
    elseif c.instr == "jmp"  # My favourite instruction :)
        p.pc += c.offset
    else
        println("invalid instruction at $(p.pc): $c")   # Hope to never see this output
    end
end

function runandwatch(p::Processor)
    # This function runs a program and watches for an instruction executed for a second time
    seen = zeros(Bool,length(p.mem))
    while !seen[p.pc]
        seen[p.pc] = true
        execinstr(p)
    end
    println("Instr at $(p.pc) already executed, acc = $(p.acc)")
end

function initproc(file)
    # create a processor and read its program from a text file
    function makeinstr(l)
        m = match(r"(\w+) ((\+|\-)\d+)",l)
        return Memcell(m.captures[1], parse(Int, m.captures[2]))
    end
    f = open(file)
    prog = readlines(f)
    close(f)
    mem = makeinstr.(prog)     # I can't stop it, I have to use a vectorised instruction somewhere :)
    return Processor(0,1,mem)
end

testp = initproc("test.txt")
```




    Processor(0, 1, Memcell[Memcell("nop", 0), Memcell("acc", 1), Memcell("jmp", 4), Memcell("acc", 3), Memcell("jmp", -3), Memcell("acc", -99), Memcell("acc", 1), Memcell("jmp", -4), Memcell("acc", 6)])




```julia
runandwatch(testp)
```

    Instr at 2 already executed, acc = 5
    

Looks OK, go for the real thing


```julia
realp = initproc("input.txt")
runandwatch(realp)
```

    Instr at 459 already executed, acc = 1939
    

Out of pure curiosity, how many instructions are there in the program and how many of them got executed?


```julia
function runandwatch(p::Processor)
    # This function runs a program and watches for an instruction executed for a second time
    seen = zeros(Bool,length(p.mem))
    while !seen[p.pc]
        seen[p.pc] = true
        execinstr(p)
    end
    println("Instr at $(p.pc) already executed, acc = $(p.acc)")
    println("Total program length: $(length(p.mem))")
    println("Instructions executed: $(count(seen))")
end
realp.acc = 0
realp.pc = 1
runandwatch(realp)
```

    Instr at 459 already executed, acc = 1939
    Total program length: 636
    Instructions executed: 196
    

## Part 2
Need to change the execute function slightly, to add the termination condition. Function `tryfix` is the one that actually solves the problem: it creates copies of the original processor in a loop, each copy gets one instruction changed, until it finds one that terminates. 

Note how `deepcopy` is needed, simple `copy` would only copy reference to the `mem` array, because arrays are mutable. I could of course avoid all that copying by operating on one processor state and changing back the instruction after each try, but well, this was faster to write, and the memory array is not that long to make a significant difference.

Also, if I wanted to maximally optimise the function, I'd only try to change instructions that have actually been executed (i.e. those for which `seen` flag is set after the initial run). But again, no point to do it in a single-use program. Also, `jmp +1` instructions sprinkled liberally over the competition code (mine has 20 of them, out of 636 instructions total), are equivalent to `nop` and thus could be excluded from changing.


```julia
function runandwatchorstop(p::Processor)
    # This function runs a program and watches for and instruction executed second time, or 
    # program termination by the pc running out of the memory bound.
    seen = zeros(Bool,length(p.mem))
    while p.pc ≤ length(p.mem) && !seen[p.pc]
        seen[p.pc] = true
        execinstr(p)
    end
    return p.pc > length(p.mem)   # Function returns true if program terminated
end

function tryfix(p::Processor)
    for i = 1:length(p.mem)
        if p.mem[i].instr == "acc"
            continue
        end
        w = deepcopy(p)           # this is our "working" processor state, that will be modified
        if w.mem[i].instr == "nop"
            w.mem[i] = Memcell("jmp", w.mem[i].offset) 
            # cost of making Memcell inmutable: it has to be completely replaced, can't change a single field
        else
            w.mem[i] = Memcell("nop", w.mem[i].offset)
        end
        if runandwatchorstop(w)
            println("Works for address $i, last acc $(w.acc)")
            break
        end
    end
end

function resetproc(p::Processor)
    # Resets the processor to initial state
    p.acc = 0
    p.pc = 1
end

resetproc(testp)
tryfix(testp)
```

    Works for address 8, last acc 8
    


```julia
resetproc(realp)
tryfix(realp)
```

    Works for address 414, last acc 2212
    

If we are to use the processor again, it might make sense to make `Memcell` mutable as well.
