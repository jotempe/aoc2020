# AoC 2020 day 15
North pole elves are a strange folk judging form how they amuse themselves. It is not the first time we encounter their strange number games in AoC (cf. [AoC 2018 day 9](https://adventofcode.com/2018/day/9)).

Understanding the rules of the game is the main difficulty of part one. What makes me a headache is part 2: somehow I expect to be asked about a number spoken as... \[insert some very large number here]. Or maybe we'll be asked when a given number first appears in the sequence? Will see.

Inputs are so short, that I don't read them from data files, just copied into the notebook. We know that brute force will fail in part 2, but use it nevertheless, to get the first star quickly.


```julia
function nextnum(l)
    n = length(l)
    x = l[n]
    for i = n-1:-1:1
        if x == l[i]
            return n-i
            break
        end
    end
    return 0
end

l1 = [0,3,6]
function lastnum(l,n)
    beg = length(l)+1
    for i = beg:n
        push!(l,nextnum(l))
    end
    l[end]
end
lastnum(l1,2020)
```




    436




```julia
lc = [2,15,0,9,1,20]
lastnum(lc,2020)
```




    1280



## Part 2
I was right. Brute forcing it is probably not a good idea. Might try to find some rule that allows to "predict" a given number in the sequence without actually computing the whole sequence. But given how the sequence is constructed, that would be really tricky. Or... what takes most time? Linear search through the list. How to avoid it? Use a dictionary, holding for each number the last position it had in the sequence. Once we have that, we may do away with keeping the sequence itself, it is not needed any more.


```julia
function lastnum2(l,n)
    d = Dict((l[i]=>i for i in 1:length(l)-1)) # initial dictionary. Don't put the last element in yet.
    last = l[end]
    for i in (length(l)):n-1   # i points to the last number in the existing sequence, hence n-1 as upper limit
        if haskey(d,last)
            next = i-d[last]   # last number has been called before, calculate distance
        else
            next = 0           # last number appeared for the first time, distance is 0
        end
        d[last] = i            # update dictionary
        last = next
    end
    last
end
l1 = [0,3,6]
lastnum2(l1,2020)            
```




    436




```julia
lastnum2(l1,30000000)
```




    175594



Tests OK. Need to recreate the initial sequences because the original `lastnum` function actually destroyed them (well, not exactly destroyed, extended).


```julia
lc = [2,15,0,9,1,20]
lastnum2(lc,30000000)
```




    651639



If it were not that early, I would probably have written the dictionary solution immediately...

## More
Reading about dictionaries in Julia I found a function that preallocates space for a dictionary. As part 2 of today's puzzle was the first problem that took "visible" computing time, I wonder how much could such preallocation have sped it up.

Need some functions to measure computing time. Julia has several tools for timing and I don't quite understand the differences between them, but try macro `@elapsed`.


```julia
@elapsed lastnum2(lc,30000000)
```




    2.691946811




```julia
@elapsed lastnum2(lc,30000000)
```




    2.794951142




```julia
@elapsed lastnum2(lc,30000000)
```




    2.972797924




```julia
@elapsed lastnum2(lc,30000000)
```




    2.810782962



The macro is measuring wall clock time. There exists a package called CPUTime, try that.


```julia
using CPUTime
@CPUelapsed lastnum2(lc,30000000)
```




    2.668




```julia
@CPUelapsed lastnum2(lc,30000000)
```




    2.589



Still not quite repeatable, but 2.6-2.7 seconds seems to be the average. 

So, first check if explicitly declaring types of dictionary keys and values helps. Also use the opportunity to measure the size of the final dictionary. 


```julia
function lastnum2(l,n)
    d = Dict{Int64,Int64}((l[i]=>i for i in 1:length(l)-1)) # initial dictionary. Don't put the last element in yet.
    last = l[end]
    for i in (length(l)):n-1   # i points to the last number in the existing sequence, hence n-1 as upper limit
        if haskey(d,last)
            next = i-d[last]   # last number has been called before, calculate distance
        else
            next = 0           # last number appeared for the first time, distance is 0
        end
        d[last] = i            # update dictionary
        last = next
    end
    println(length(d))
    last
end
lastnum2(lc,30000000)
```

    3611432
    




    651639



Hm, surprisingly large! over three million entries. Measure it.


```julia
@CPUelapsed lastnum2(lc,30000000)
```

    3611432
    




    2.714




```julia
@CPUelapsed lastnum2(lc,30000000)
```

    3611432
    




    2.902




```julia
@CPUelapsed lastnum2(lc,30000000)
```

    3611432
    




    2.855



That is... interesting. Let's call the result "declaring explicit dictionary type does not help". What about preallocating space?


```julia
function lastnum3(l,n)
    d = Dict{Int64,Int64}((l[i]=>i for i in 1:length(l)-1)) # initial dictionary. Don't put the last element in yet.
    sizehint!(d,4000000)
    last = l[end]
    for i in (length(l)):n-1   # i points to the last number in the existing sequence, hence n-1 as upper limit
        if haskey(d,last)
            next = i-d[last]   # last number has been called before, calculate distance
        else
            next = 0           # last number appeared for the first time, distance is 0
        end
        d[last] = i            # update dictionary
        last = next
    end
    last
end
lastnum3(lc,30000000)
```




    651639




```julia
@CPUelapsed lastnum3(lc,30000000)
```




    2.684




```julia
@CPUelapsed lastnum3(lc,30000000)
```




    2.729




```julia
@CPUelapsed lastnum3(lc,30000000)
```




    2.59



No significant difference... That, or I don't understand how to use `sizehint!`. Anyhow, below 3 seconds to build a dictionary of almost four million entries and run the loop accessing and modifying it 30 million times, not bad. 
