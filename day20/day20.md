# AoC 2020 day 20
A kind of a jigsaw puzzle. OK. what do we know? Each tile is 10x10 pixels, so the border is 10 bits long. There are 144 tiles total in the data set, so the image must be 12x12 tiles, or 109x109 pixels (I assume the borders are overlapping). For each tile I have to consider 8 possible orientations. Each side of a tile is a 10-bit number, when matching them i have to consider the number and its bit reverse. I have four sides per tile, which means a total of 576 sides. each side can be represented by a pair of numbers (straight and reversed). Some numbers are palindromic in binary notation, so in those cases both numbers will be the same.

The simplest plan to solve part 1 would be to find tiles with two neighbouring borders, that don't match any other border. But, with a total of $144\times 4\times 2 = 1152$ those "border numbers", there is no guarantee that we can find four such tiles with two "unmatched" borders. After all, there are only 1024 10-bit numbers available. Unless, of course, the puzzle author arranged it so. But in the worst case we'd need to reassemble the entire image (will probably have to anyhow in part 2) to find corner tiles. OK, let's hope we can find at least one corner (like in real jigsaw, there is a strategy to start with corners and border tiles).

So, read in the data first. The structure for in-memory data will be a dictionary, with tile number being the key and content the value.


```julia
function readin(file)
    f = open(file)
    data = Dict()
    while !eof(f)
        m = match(r"Tile (\d+):",readline(f))
        key = parse(Int,m.captures[1])
        tile = [readline(f) for i=1:10]
        data[key] = copy(tile)
        readline(f)
    end
    data
end
testset = readin("test.txt")
```




    Dict{Any,Any} with 9 entries:
      2729 => ["...#.#.#.#", "####.#....", "..#.#.....", "....#..#.#", ".##..##.#."…
      1489 => ["##.#.#....", "..##...#..", ".##..##...", "..#...#...", "#####...#."…
      2473 => ["#....####.", "#..#.##...", "#.##..#...", "######.#.#", ".#...#.#.#"…
      3079 => ["#.#.#####.", ".#..######", "..#.......", "######....", "####.#..#."…
      2311 => ["..##.#..#.", "##..#.....", "#...##..#.", "####.#...#", "##.##.###."…
      1171 => ["####...##.", "#..##.#..#", "##.#..#.#.", ".###.####.", "..###.####"…
      2971 => ["..#.#....#", "#...###...", "#.#.###...", "##.##..#..", ".#####..##"…
      1951 => ["#.##...##.", "#.####...#", ".....#..##", "#...######", ".##.#....#"…
      1427 => ["###.##.#..", ".#..#.##..", ".#.##.#..#", "#.#.#.##.#", "....#...##"…



Looks fine. Now a function that for each tile calculates the numeric value of its borders, and also a bit reverse of that value.


```julia
function val(s)
    # For a string of . and # calculate its value by replacing `#` with 1
    val = 0
    for c in s
        val <<= 1
        if c=='#'
            val += 1
        end
    end
    val
end

function breverse(in::Int)
    # Reverse bits in the input 10-bit number
    val = 0
    for i in 1:10
        val <<=1
        val |= in & 1
        in >>= 1
    end
    val
end

function bvals(tile)
    # Calculate the 8 "border values" of a tile
    res = zeros(Int,8)
    res[1] = val(tile[1])
    res[3] = val(tile[10])
    res[5] = val([tile[i][1] for i in 1:10])
    res[7] = val([tile[i][end] for i in 1:10])
    # Now add reverses of those values
    for i in 2:2:8
        res[i] = breverse(res[i-1])
    end
    res
end

testset[2729]
```




    10-element Array{String,1}:
     "...#.#.#.#"
     "####.#...."
     "..#.#....."
     "....#..#.#"
     ".##..##.#."
     ".#.####..."
     "####.#.#.."
     "##.####..."
     "##..#.##.."
     "#.##...##."




```julia
bvals(testset[2729])
```




    8-element Array{Int64,1}:
      85
     680
     710
     397
     271
     962
     576
       9



Looks OK. Construct a second dictionary that, for each tile, contains the 8 values


```julia
function makebvals(tiles)
    res = Dict()
    for (k,v) in tiles
        res[k] = bvals(v)
    end
    res
end
makebvals(testset)
```




    Dict{Any,Any} with 9 entries:
      2729 => [85, 680, 710, 397, 271, 962, 576, 9]
      1489 => [848, 43, 948, 183, 565, 689, 18, 288]
      2473 => [542, 481, 234, 348, 966, 399, 116, 184]
      3079 => [702, 501, 184, 116, 616, 89, 264, 66]
      2311 => [210, 300, 231, 924, 498, 318, 89, 616]
      1171 => [966, 399, 24, 96, 902, 391, 288, 18]
      2971 => [161, 532, 85, 680, 456, 78, 565, 689]
      1951 => [710, 397, 564, 177, 841, 587, 498, 318]
      1427 => [948, 183, 210, 300, 576, 9, 234, 348]



And now find tiles with some borders that do not match any other tile


```julia
function findbc(bvals)
    corners = []
    borders = []
    interior = []
    # This is rather inefficient because it loops twice over all the tiles, but who cares with thit tile count
    for (k,t) in bvals      
        nmatch = 0
        for v in [t[i] for i = 1:2:7] # Match only "straight" values, otherwise would double count matches
            for (k1,t1) in bvals      # look for tiles with a border matching this one
                if k1==k              # do not try to match yourself
                    continue
                end
                if v in t1
                    nmatch += 1
                    break
                end
            end
        end
        if nmatch == 2
            # A tile whose borders match only two other tiles are surely corners
            push!(corners,k)
        elseif nmatch == 3
            # If one side matches no other tile, it must be a border tile
            push!(borders,k)
        elseif nmatch == 4
            # Those that have a match on every side are "interior"
            push!(interior,k)
        else
            println("problem tile $k nmatch $nmatch")
        end
    end
    corners,borders,interior
end
findbc(makebvals(testset))
```




    (Any[3079, 1171, 2971, 1951], Any[2729, 1489, 2473, 2311], Any[1427])



So, for the test dataset we can find corners and borders easily, what about the competition dataset?


```julia
inset = readin("input.txt")
inbvals = makebvals(inset)
corners, borders, interior = findbc(inbvals)
```




    (Any[1987, 2647, 3643, 3089], Any[1993, 2441, 2539, 2621, 1151, 1429, 3821, 2351, 1709, 1531  …  2969, 2377, 2957, 2777, 2341, 2579, 1021, 2663, 1597, 2837], Any[2843, 3407, 2311, 2917, 1249, 1237, 2383, 1741, 3467, 3229  …  1187, 2531, 2153, 2791, 3001, 1697, 1321, 2087, 1423, 1433])




```julia
corners[1]*corners[2]*corners[3]*corners[4]
```




    59187348943703



## Part 2
OK... reassembly is indeed required. This will be a long and rough day. There are two problems to solve here:
1. reassemble the image
1. find "sea monsters" and count the remaining tiles

### Phase 1: reassemble image
Each tile turns out to have only 8x8 bits of useful information. So the entire image is 96x96 bits. As later we'll have to bitmask slices of the array, a 96x96 bit (actually boolean) matrix seems the best way of representing the image.

For the assembly phase construct a 12x12 matrix of tile keys.

First, count the border pieces we have found.


```julia
length(borders)
```




    40



Great, we have 4 corners and 40 borders, that means we can reassemble using the classic jigsaw "assemble borders first" strategy. Once we have the complete border reassembled, we can proceed in such way, that each added tile has to match two existing ones, and thus will hopefully be unique.

Start with constructing a new tile list (still a dictionary), containing tiles represented as 8x8 boolean matrix and four "border match" values (discard the reverses, will compute them dynamically, as needed). Will need tile flip and rotate operations that operate on the bit array and correctly update the border "match" values. 


```julia
struct Btile
    pixel ::Array{Bool,2}
    bm ::Array{Int}        # border match values ordered "up", "down", "left", "right"
end

function makebits(tile)
    # Build a boolean array from the list of strings
    pxl = Array{Bool}(undef,(8,8))
    for i=1:8, j=1:8
        pxl[i,j] = tile[i+1][j+1]=='#'
    end
    pxl
end
function makebtiles(tiles,bvals)
    # The function turns a dictionary of tiles represented as string vectors to tiles represented
    # by Btile structures
    btiles = Dict()
    for i in keys(tiles)
        btiles[i] = Btile(makebits(tiles[i]),[bvals[i][j] for j=1:2:7])
    end
    btiles
end

btvals = makebvals(testset)
btest = makebtiles(testset, btvals)
```




    Dict{Any,Any} with 9 entries:
      2729 => Btile(Bool[1 1 … 0 0; 0 1 … 0 0; … ; 1 0 … 0 0; 1 0 … 1 0], [85, 710,…
      1489 => Btile(Bool[0 1 … 1 0; 1 1 … 0 0; … ; 1 0 … 1 1; 0 1 … 0 1], [848, 948…
      2473 => Btile(Bool[0 0 … 0 0; 0 1 … 0 0; … ; 1 1 … 1 0; 1 0 … 0 1], [542, 234…
      3079 => Btile(Bool[1 0 … 1 1; 0 1 … 0 0; … ; 0 1 … 0 0; 0 1 … 0 0], [702, 184…
      2311 => Btile(Bool[1 0 … 0 0; 0 0 … 0 1; … ; 0 1 … 1 0; 1 1 … 0 1], [210, 231…
      1171 => Btile(Bool[0 0 … 0 0; 1 0 … 0 1; … ; 0 1 … 1 1; 1 1 … 0 0], [966, 24,…
      2971 => Btile(Bool[0 0 … 0 0; 0 1 … 0 0; … ; 0 1 … 1 1; 0 1 … 1 1], [161, 85,…
      1951 => Btile(Bool[0 1 … 0 0; 0 0 … 0 1; … ; 1 1 … 0 1; 0 1 … 1 0], [710, 564…
      1427 => Btile(Bool[1 0 … 1 0; 1 0 … 0 0; … ; 1 0 … 0 1; 0 1 … 1 0], [948, 210…



Will need a bunch of manipulation functions for tiles: rotate, flip... For the pixel matrix itself they already exist in base Julia, but we need to transform the border match values correctly.


```julia
function rotl(btile)
    # Function rotates a tile left 90 degrees
    pxl = rotl90(btile.pixel)
    # The order of border matching numbers is: up, down, left, right. MSB is left and up
    # When rotating left Up becomes left reversed, left becomes down etc.
    mup = btile.bm[4]
    mdn = btile.bm[3]
    mlf = breverse(btile.bm[1])
    mrg = breverse(btile.bm[2])
    Btile(pxl,[mup,mdn,mlf,mrg])
end
function flipdiag(btile)
    # function flips tile diagonally
    pxl = transpose(btile.pixel)
    mup = btile.bm[3]
    mdn = btile.bm[4]
    mlf = btile.bm[1]
    mrg = btile.bm[2]
    Btile(pxl,[mup,mdn,mlf,mrg])
end
function fliphor(btile)
    # function fips tile around horizontal axis
    pxl = reverse(btile.pixel,dims=1)
    mup = btile.bm[2]
    mdn = btile.bm[1]
    mlf = breverse(btile.bm[3])
    mrg = breverse(btile.bm[4])
    Btile(pxl,[mup,mdn,mlf,mrg])
end

btest[2729].pixel
```




    8×8 Array{Bool,2}:
     1  1  1  0  1  0  0  0
     0  1  0  1  0  0  0  0
     0  0  0  1  0  0  1  0
     1  1  0  0  1  1  0  1
     1  0  1  1  1  1  0  0
     1  1  1  0  1  0  1  0
     1  0  1  1  1  1  0  0
     1  0  0  1  0  1  1  0




```julia
rotl(btest[2729]).pixel
```




    8×8 Array{Bool,2}:
     0  0  0  1  0  0  0  0
     0  0  1  0  0  1  0  1
     0  0  0  1  1  0  1  1
     1  0  0  1  1  1  1  0
     0  1  1  0  1  0  1  1
     1  0  0  0  1  1  1  0
     1  1  0  1  0  1  0  0
     1  0  0  1  1  1  1  1




```julia
flipdiag(btest[2729]).pixel
```




    8×8 Array{Bool,2}:
     1  0  0  1  1  1  1  1
     1  1  0  1  0  1  0  0
     1  0  0  0  1  1  1  0
     0  1  1  0  1  0  1  1
     1  0  0  1  1  1  1  0
     0  0  0  1  1  0  1  1
     0  0  1  0  0  1  0  1
     0  0  0  1  0  0  0  0



OK, matrix manipulation itself works, and if I messed up with "border match" transformations, it will eventually show up. Combining transpose with rotations we can get all possible orientations of a tile. In order to assemble the image we'll need a function to tell if a piece "fits" given border value.


```julia
fits(btile, value) = value in btile.bm || value in breverse.(btile.bm)
btest[2729].bm
```




    4-element Array{Int64,1}:
      85
     710
     271
     576




```julia
fits(btest[2729],710)
```




    true




```julia
fits(btest[2729],9)
```




    true



OK, let's try how that will work on the test data set. Pick the first corner, make it "upper left". Orient it so, that the two borders that don't match anything are left and up. Pick a piece that fits its right border, from the list of "border" pieces, rotate and flip it as needed to have the fitting border on the left side of the piece and add it to the image being built. Repeat to have the upper border up to the corner (in the test dataset it will only be done once). Then find the final corner piece so, that its left fits the right of the last border piece.

Then do the same vertically, for first and last columns (this time down of already placed tile has to match up on the next tile). Finally assemble the lower border. In principle we could start assembling by doing upper and left borders only, then every next tile placed has to fit two others. But since I already divided the tiles into corners, borders and interior, I know from which case to draw when looking of a fit.

Finally fill the interior, row by row, always looking for a match on both, the tile to the left and the tile above. 

The `ordered` dictionary contains the list of Btile structures with correct orientation, and the returned table contains arrangement of tiles in the final image.

The `orient` function is rather stupid, because it tries simply all tile orientations until it finds one that fits, in principle knowing on which position the matched border is and if it is reversed allows us to determine the correct orientation, but I'm lazy today. No, not lazy, I'm getting tired with this puzzle. Another sign that I'm getting tired is that the above procedure ignores the possibility, that there may be more than one matching border tile, so i should in principle be checking if a "match" in the above sense has the unmatched side on the exterior of the image. Well, if that happens, then we'll eventually end up with no matching tile and then we'll redo the work more carefully.


```julia
function assemble(btiles,corner,border,interior)
    function orient(tile, tomatch, which)
        # rotates and flips a tile so, that border indicated by 'which' has match value 'tomatch' and return 
        function tryrot()
            # try rotations by 90, 180 and 270 degrees, until one fits
            for i= 1:3
                tile = rotl(tile)
                if tile.bm[which] == tomatch
                    break
                end
            end
        end
        if tile.bm[which] ≠ tomatch  # maybe the piece is already correcty oriented?
            tryrot()                 # no, try rotating it
            if tile.bm[which] ≠ tomatch  # stil no match, so try to flip and then rotate
                tile = flipdiag(tile)
            end
            if tile.bm[which] ≠ tomatch
                tryrot()
            end
        end
        return tile
    end
    function orienttlcorner()
        # This function orients the upper left corner tile, co that unmatched sides are up and left.
        function hasfit(which)
            tofit = ctile.bm[which]
            for t in border
                if fits(btiles[t],tofit)
                    return true
                end
            end
            return false
        end
        ctile = btiles[corner[1]]
        while !hasfit(4)
            # Rotate until right side fits some border tlle
            ctile = rotl(ctile)
        end
        if !hasfit(2)
            # if in that position lower border has no match, flip horizontally
            ctile = fliphor(ctile)
        end
        return ctile
    end                
                    
    function assembleleft(row)
        # assemble hrizontal border by matchig with tule to the left
        for col in 2:(side-1)
            tomatch = oriented[ttable[row,col-1]].bm[4]
            for t in border
                # tiles in 'oriented' dictionary have already been used.
                if !haskey(oriented,t) && fits(btiles[t],tomatch)
                    ttable[row,col] = t
                    oriented[t] = orient(btiles[t],tomatch,3)
                    break
                end
            end
        end
        if ttable[row,side] == 0
            # if needed, finish with finding the corner.
            tomatch = oriented[ttable[row,side-1]].bm[4]
            for t in corner
                if !haskey(oriented,t) && fits(btiles[t],tomatch)
                    ttable[row,side] = t
                    oriented[t] = orient(btiles[t],tomatch,3)
                    break
                end
            end
        end
    end
    
    function assembleup(col)
        # Assemble vertical border top-down
        for row in 2:(side-1)
            tomatch = oriented[ttable[row-1,col]].bm[2]
            for t in border
                if !haskey(oriented,t) && fits(btiles[t],tomatch) 
                    ttable[row,col] = t
                    oriented[t] = orient(btiles[t],tomatch,1)
                    break
                end
            end
        end
        if ttable[side,col] == 0
            tomatch = oriented[ttable[side-1,col]].bm[2]
            for t in corner
                if !haskey(oriented,t) && fits(btiles[t],tomatch)
                    ttable[side,col] = t
                    oriented[t] = orient(btiles[t],tomatch,1)
                    break
                end
            end
        end
    end
    
    function assemblerest()
        # Assemble interior
        for row in 2:(side-1), col in 2:(side-1)
            tomatch1 = oriented[ttable[row,col-1]].bm[4]
            tomatch2 = oriented[ttable[row-1,col]].bm[2]
            # find a tile that matches two neighbours, but use only the left one to define orientation    
            for t in interior
                if !haskey(oriented,t) && fits(btiles[t],tomatch1) && fits(btiles[t],tomatch2)
                    ttable[row,col] = t
                    oriented[t] = orient(btiles[t],tomatch1,3)
                    break
                end
            end
        end
    end
            
    side = Int(sqrt(length(btiles)))
    ttable = zeros(Int,(side,side))
    oriented = Dict()                
    ttable[1,1] = corner[1]
    oriented[corner[1]] = orienttlcorner()
    assembleleft(1)
    assembleup(1)
    assembleup(side)
    assembleleft(side)
    assemblerest()
    ttable, oriented
end
tc,tb,ti = findbc(btvals)
```




    (Any[3079, 1171, 2971, 1951], Any[2729, 1489, 2473, 2311], Any[1427])




```julia
tt, tor = assemble(btest,tc,tb,ti)
tt
```




    3×3 Array{Int64,2}:
     3079  2473  1171
     2311  1427  1489
     1951  2729  2971



Function `makeimage` assembles oriented times into one image, according to the ordering given by table returned by `assemble`.


```julia
function makeimage(ttable, oriented)
    function makerow(row)
        imrow = oriented[ttable[row,1]].pixel
        for t in ttable[row,2:end]
            imrow = hcat(imrow,oriented[t].pixel)
        end
        imrow
    end
    img = makerow(1)
    for i in 2:length(ttable[1,:])
        img = vcat(img,makerow(i))
    end
    img
end
timage = makeimage(tt,tor)
```




    24×24 Array{Bool,2}:
     1  0  0  1  1  1  0  0  0  0  1  1  0  1  0  0  0  1  1  0  1  1  0  1
     1  0  0  0  1  0  0  0  0  0  1  0  0  1  1  0  0  0  1  1  1  0  1  1
     1  0  0  0  0  1  1  0  0  1  0  1  1  1  1  1  1  1  1  1  0  0  1  1
     1  0  1  1  1  1  1  0  0  1  0  1  0  0  0  1  1  0  0  1  0  0  0  0
     1  0  1  0  0  1  1  0  1  1  1  1  1  1  1  1  0  0  1  0  0  1  1  0
     0  0  1  1  0  1  0  0  1  0  0  1  0  1  1  1  1  1  1  1  0  1  1  1
     0  1  1  1  0  1  1  1  0  1  1  1  1  1  1  1  0  0  1  1  1  1  1  0
     1  0  1  1  1  0  0  0  1  0  1  1  0  0  0  1  0  1  1  1  1  1  1  0
     1  0  1  1  1  0  1  0  0  1  1  1  1  0  0  0  1  1  0  0  1  0  0  0
     0  1  0  1  1  0  0  0  1  0  1  1  0  1  0  1  0  1  1  1  0  0  0  1
     1  0  0  0  1  0  0  0  1  1  1  0  0  1  1  1  1  0  0  0  0  1  1  0
     0  0  1  1  0  1  1  0  1  1  1  0  0  0  0  0  1  0  1  1  0  0  1  0
     0  0  0  0  1  0  1  1  0  1  0  1  1  1  1  1  0  0  0  0  1  0  0  0
     0  0  1  0  1  1  0  0  0  1  1  1  0  0  1  0  1  1  1  1  1  0  0  1
     1  1  0  0  0  1  0  0  1  0  0  0  0  1  0  0  1  0  0  0  1  1  1  1
     1  0  1  1  1  1  0  1  0  1  0  0  0  0  1  1  0  1  0  0  1  0  1  0
     0  1  1  1  0  1  1  0  0  0  0  0  1  0  0  0  1  1  1  0  1  0  0  0
     1  0  1  1  0  1  0  0  1  0  1  0  0  1  0  0  1  1  0  1  0  1  0  0
     0  0  0  1  0  1  0  0  1  1  0  1  1  0  0  0  1  0  0  1  0  0  1  1
     0  0  1  1  0  1  1  1  0  1  1  1  1  0  0  1  0  1  1  1  1  0  1  1
     1  0  1  0  1  1  0  1  1  1  0  1  0  1  1  0  1  1  0  1  1  1  1  1
     0  1  0  1  0  0  0  1  0  1  1  1  0  0  0  1  0  1  1  0  1  1  0  0
     1  1  1  1  1  0  0  1  0  0  1  0  1  0  1  1  1  1  0  0  1  0  1  0
     0  1  1  1  1  0  0  0  1  1  1  1  1  0  0  1  0  0  0  1  1  1  0  0



Works for test dataset, try with competition input.


```julia
inbvals = makebvals(inset)
intiles = makebtiles(inset, inbvals)
corners, borders, interior = findbc(inbvals)
intab, inoriented = assemble(intiles, corners, borders, interior)
intab
```




    12×12 Array{Int64,2}:
     1987  2539  2711  1021  2441  2477  1429  1171  3181  2243  2579  3089
     1931  1297  2267  2179  3803  2851  2749  1523  1367  3691  2707  1427
     1597  1187  1321  3571  2917  3023  1327  3923  2731  3469  3389  2341
     2621  1831  2767  1949  1291  2137  3677  1697  2287  1087  3457  3371
     2417  2087  1663  2617  1063  3169  3631  1423  2381  3779  2161  3821
     3449  3989  3607  3001  1867  1373  1163  1567  3733  1997  2609  2663
     1723  3833  3407  1823  3229  1213  2531  1051  3911  2347  2423  2971
     2789  2029  2311  2153  1559  2273  3919  3373  2089  1451  3907  2837
     2957  3019  1249  3559  3347  1237  1433  2791  2897  1303  1741  1993
     1709  1583  2833  3637  1973  3467  2143  1951  3041  3527  1033  2351
     1571  2843  2383  3331  1669  1733  2659  2063  3719  3461  2003  1531
     3643  1019  2377  1301  1151  1871  2969  1873  2777  2819  3433  2647




```julia
image = makeimage(intab,inoriented)
```




    96×96 Array{Bool,2}:
     0  0  0  0  0  0  0  0  0  0  1  1  0  …  0  1  0  0  0  0  1  1  0  0  0  0
     0  0  0  0  0  0  0  0  0  0  0  0  1     0  0  0  0  0  0  1  0  0  0  0  0
     0  1  0  0  0  0  0  0  0  0  0  1  0     1  0  0  0  0  1  0  0  0  0  1  0
     0  0  0  0  0  0  0  0  1  1  0  0  0     0  0  0  0  1  0  0  0  1  0  0  1
     0  0  0  0  0  0  0  0  0  1  0  0  0     0  0  0  0  0  0  0  0  1  1  0  1
     0  0  0  0  0  1  0  0  0  0  1  0  0  …  0  0  0  0  0  1  0  0  1  0  0  0
     1  0  0  0  1  0  0  0  0  0  0  0  1     0  1  0  0  1  0  0  1  0  0  0  1
     1  0  0  0  0  1  0  0  0  0  1  0  0     0  1  0  1  0  0  0  0  0  0  0  0
     0  0  1  0  0  1  0  0  0  0  1  0  0     0  0  0  0  0  1  0  0  0  0  1  0
     0  0  0  0  0  0  1  0  0  0  0  1  0     0  0  1  0  0  0  0  1  0  0  1  0
     0  0  0  0  1  0  0  0  0  0  0  0  0  …  0  0  0  0  1  0  0  0  1  1  0  0
     0  0  0  1  0  0  0  1  0  1  0  0  1     0  0  0  0  1  0  0  0  1  0  0  0
     0  0  0  0  1  0  0  0  0  0  0  1  0     0  0  0  0  0  0  1  1  0  0  0  1
     ⋮              ⋮              ⋮        ⋱     ⋮              ⋮              ⋮
     0  0  0  0  0  0  0  1  1  0  0  0  0     0  0  0  0  0  1  0  0  0  0  0  1
     0  1  0  0  0  0  0  0  0  1  0  1  0  …  0  0  1  0  0  0  0  0  1  0  1  0
     0  0  0  0  0  0  0  0  1  0  0  0  0     0  0  0  0  0  0  0  0  0  0  0  0
     0  0  0  0  1  0  0  1  0  0  0  0  0     1  0  1  0  0  0  1  0  1  0  0  0
     1  1  1  0  0  0  1  0  1  1  0  0  0     0  0  0  0  1  0  0  0  1  1  0  0
     0  0  0  0  0  0  0  0  0  0  0  0  0     0  1  0  0  0  0  1  0  0  0  0  0
     1  0  1  1  0  0  0  0  0  0  1  0  0  …  1  0  0  0  0  1  0  0  0  0  0  0
     1  0  0  1  0  0  1  0  0  0  0  0  0     0  0  0  0  0  1  0  0  0  0  0  0
     1  0  0  0  1  0  0  0  0  0  1  0  0     1  0  1  1  0  0  0  0  0  0  0  0
     0  0  0  0  0  0  1  0  1  0  1  0  0     0  0  0  0  1  0  1  0  0  0  0  0
     0  0  0  0  0  0  0  0  1  0  0  0  0     0  0  0  0  0  1  0  0  0  1  1  1
     0  1  0  1  0  0  0  0  0  0  0  1  0  …  0  0  0  0  0  0  0  0  0  0  0  0



### Find monsters
Read the "monster" image into an array. For all possible locations logically and the monster image with the analysed image. If all the bits of the conjunction are set, we got a monster. 

BTW: only now did I learn that `readlines()` and some other I/O functions accept the file name in place of the file handle. So many explicit file opens and closes in the previous days were simply unnecessary. Oh well, cost of using a language you don't know: you sometimes do things in a roundabout way.


```julia
function readmon(file)
    inp = readlines(file)
    mon = zeros(Bool,(length(inp),length(inp[1])))
    for i in 1:length(inp), j=1:length(inp[1])
        if inp[i][j] == '#'
            mon[i,j] = true
        end
    end
    mon
end
mon = readmon("monster.txt")
```




    3×20 Array{Bool,2}:
     0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  1  0
     1  0  0  0  0  1  1  0  0  0  0  1  1  0  0  0  0  1  1  1
     0  1  0  0  1  0  0  1  0  0  1  0  0  1  0  0  1  0  0  0




```julia
monh = length(mon[:,1])-1
monw = length(mon[1,:])-1
moncnt = count(mon)
monh, monw, moncnt
```




    (2, 19, 15)



The following function overlays the monster image on the input image, at every possible position, and finds matches.


```julia
function findmonsters(image,monster)
    side = length(image[1,:])
    nmon = 0
    for i in 1:(side-monh), j=1:(side-monw)
        over = count(mon .& image[i:(i+monh),j:(j+monw)])
        if over==moncnt
            println("monster at $i $j")
            nmon += 1
        end
    end
    nmon
end
findmonsters(timage,mon)
```




    0



The following function repeats `findmonsters` on all possible rotations of the input image. If it finds none, we'll try to flip and rotate next


```julia
function checkimgs(image)
    println(findmonsters(image,mon))
    work = rotl90(image)
    println(findmonsters(work,mon))
    work = rot180(image)
    println(findmonsters(work,mon))
    work = rotr90(image)
    println(findmonsters(work,mon))
end
checkimgs(timage)
    
```

    0
    0
    0
    0
    


```julia
timage2 = reverse(timage,dims=1)
checkimgs(timage2)
```

    monster at 3 3
    monster at 17 2
    2
    0
    0
    0
    


```julia
rough = count(timage)-2*moncnt
```




    273



Right. Do it for the actual image


```julia
checkimgs(image)
```

    0
    0
    0
    0
    


```julia
image2=reverse(image,dims=1)
checkimgs(image2)
```

    0
    monster at 2 13
    monster at 3 74
    monster at 4 41
    monster at 6 18
    monster at 11 42
    monster at 13 73
    monster at 14 9
    monster at 18 26
    monster at 19 53
    monster at 23 11
    monster at 25 70
    monster at 29 46
    monster at 30 72
    monster at 34 2
    monster at 35 70
    monster at 39 2
    monster at 40 30
    monster at 41 67
    monster at 46 2
    monster at 48 43
    monster at 52 15
    monster at 52 50
    monster at 56 64
    monster at 57 7
    monster at 60 29
    monster at 63 53
    monster at 67 23
    monster at 68 61
    monster at 73 3
    monster at 73 36
    monster at 73 64
    monster at 80 26
    monster at 80 49
    monster at 86 58
    monster at 87 8
    monster at 91 34
    36
    0
    0
    


```julia
count(image)-36*moncnt
```




    1565



This was exhausting. I almost lost interest. And yes, I know that what I have written is extremely ugly in places and misses many wonderful features of Julia. But I've had enough. So, no discussion what could have been done better today.
