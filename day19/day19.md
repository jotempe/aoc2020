# AoC 2020 day 19
It is tempting to build one regular expression out of those rules and try to match it. Sounds like one of them, very stupid ideas, because for the competition rule set the regex would be huge. But it is some idea at least.

OK, start with reading in the test data and see what comes up.


```julia
function readin(file)
    f = open(file)
    rules = Dict()
    while (l=readline(f)) ≠ ""    # Empty line marks transition from rules to messages to validate
        # read in the rules
        m = match(r"^(\d+): (.+)$",l)
        rules[parse(Int,m.captures[1])] = m.captures[2]
    end
    messages = []
    while !eof(f)
       push!(messages,readline(f))
    end
    close(f)
    rules,messages
end
tr,tm = readin("test.txt")
```




    (Dict{Any,Any}(0 => "4 1 5",4 => "\"a\"",2 => "4 4 | 5 5",3 => "4 5 | 5 4",5 => "\"b\"",1 => "2 3 | 3 2"), Any["ababbb", "bababa", "abbbab", "aaabbb", "aaaabbb"])



Another one of them, probably very stupid ideas is: build recursively a complete list of all possible matching strings and check the input against it. Reject it, the list would probably be really really long for real data, each alternative multiplies the number of matching strings by two...

OK, lets see: rules are either:
1. single character
1. a list of rules
1. two lists of rules separated by `|`
(the alternatives appear to be always two-way in the data)

Try this: replace all occurrences of single character rules with the characters they represent. Replace a list of single characters with a string. So, first parse all the rules into either a string, a list, or two lists. Function `transf1` transforms the rules into that form


```julia
function parserule(r)
    if (m=match(r"\"(a|b)\"", r)) !== nothing
        return String(m.captures[1])
    elseif occursin("|",r)
        m=match(r"(.+)\|(.+)",r)   # For alternative recurse with two sublists
        return [parserule(m.captures[1]),parserule(m.captures[2])]
    else
        l = []
        while (m=match(r"(\d+)(.*)",r)) !== nothing   # I have a feeling this could be simpler
            push!(l,parse(Int,m.captures[1]))
            r = m.captures[2]
        end
        return l
    end
end
function transf1(rules)
    rt = Dict()
    for r in rules
        rt[r[1]] = parserule(r[2])
    end
    rt
end
ttr = transf1(tr)
```




    Dict{Any,Any} with 6 entries:
      0 => Any[4, 1, 5]
      4 => "a"
      2 => Array{Any,1}[[4, 4], [5, 5]]
      3 => Array{Any,1}[[4, 5], [5, 4]]
      5 => "b"
      1 => Array{Any,1}[[2, 3], [3, 2]]



Now stare at those rules hard. The correct scientific way of doing this would probably be to build some tree and match strings going through its branches. I'm too old to figure out quickly how to do it, so go back to the first stupid idea: build a regular expression equivalent to those rules and see how fast will it match the input strings. 

The function below builds the regular expression recursively, replacing each rule by a string that represents it in regex form. Thus a literal string ist just inserted, a list is represented by joining string representations of list elements and an alternative by `(...|...)`. To avoid lots of recursion, each rule is replaced by its string representation, once it is known. So next time it is needed, it can be simply copied.


```julia
function buildregex(rules,rule)
    if isa(rule,String)
        return rule
    elseif isa(rule[1],Int)
        # list of rules
        s = ""
        for j in rule
            x = buildregex(rules,rules[j])
            rules[j] = x
            s = s*x
        end
        return s
    else
        x1 = buildregex(rules,rule[1])
        x2 = buildregex(rules,rule[2])
        s = "(" * x1 * "|" * x2 * ")"
        return s
    end
end
r = buildregex(ttr,ttr[0])
ttr[0] = r
```




    "a((aa|bb)(ab|ba)|(ab|ba)(aa|bb))b"




```julia
ttr
```




    Dict{Any,Any} with 6 entries:
      0 => "a((aa|bb)(ab|ba)|(ab|ba)(aa|bb))b"
      4 => "a"
      2 => "(aa|bb)"
      3 => "(ab|ba)"
      5 => "b"
      1 => "((aa|bb)(ab|ba)|(ab|ba)(aa|bb))"



Yep, the string representation looks OK, apply it to the example strings, after adding requirement to match the whole string, from begin to end.


```julia
x = Regex("^"*ttr[0]*"\$")
match.(x,tm)
```




    5-element Array{Union{Nothing, RegexMatch},1}:
     RegexMatch("ababbb", 1="babb", 2=nothing, 3=nothing, 4="ba", 5="bb")
     nothing
     RegexMatch("abbbab", 1="bbba", 2="bb", 3="ba", 4=nothing, 5=nothing)
     nothing
     nothing




```julia
count(a->match(x,a)!==nothing, tm)
```




    2



Great, so, draw a deep breath and see how it goes with my real input.


```julia
ir,im = readin("input.txt")
```




    (Dict{Any,Any}(68 => "48 127 | 40 116",124 => "116 57 | 127 91",2 => "116 117 | 127 72",89 => "57 127 | 33 116",11 => "42 31",39 => "12 116 | 84 127",46 => "134 116 | 62 127",134 => "116 117",85 => "126 127 | 133 116",25 => "57 116 | 54 127"…), Any["babababababababaabbbbbabaaabbabbabbabbaa", "abbbbbaababbbabbbbbaaaaaaaababbbaabbabab", "bbaaaaababababbaaaaabbababbabaabaaaaaaabaaaaaaaa", "aabaabbaaaaaabaaaaaaabaabaabbbbb", "bbaababbaaabaabaababbaaaabaababbaabbbaababbaaabababaabab", "bbabbabbabaabbaabbbbaabbbbaabbaa", "baaaabbbabaaababbababbbbababbbbbaaaabaabaabbbbaa", "aaaabbabbaaaabbbaabaaabbaaabbbbababaabab", "bbbaabaabbaabaaaaaababbbbaaaabbaaabaaabbbbbbbbbbbababbab", "aabaabbaaabaaaabbbbaababbaabababaabbbbab"  …  "ababbbbbbbbbbabbbabbaabbbbbaabababababbabbaabbabaaaabbbbabbbbaab", "abaabbaaabbababaabbbaabb", "ababaaaaabbabbbababaaaba", "baabaabbabaabbaabaababaaabbbbbbbaababaaabaaabbaaabaabbbb", "baaabbabaaabbbbbaababaabbabaaabaaaababaa", "bbaaabbbbbbbaaabaaaaaaba", "aabaaaababbaaaabbbbbbabbaabbaaba", "abaabbababbaaaabbababbbaaaabbabb", "bbaaababbaabbbababaaaaabbababaabaaaabbaaababbaaaaaaaababaaabbaab", "aababbbabababbbaabbbabba"])




```julia
itr = transf1(ir)
r = buildregex(itr,itr[0])
itr[0] = r
```




    "((b((a(b((ab|ba)a|(aa|ab)b)|a((a|b)(a|b)b|aaa))|b((abb|b(bb|ba))b|(b(bb|ba)|a(aa|ab))a))b|(((aba|(aa|ab)b)b|((aa|b(a|b))a|(aa|ba)b)a)b|(b(aa|b(a|b))(a|b)|a(aab|baa))a)a)|a(b(((b(ab|bb)|a(aa|ba))b|(b(ab|bb)|abb)a)b|(a((ab|ba)b|(aa|b(a|b))a)|b((aa|b(a|b))b|(aa|ab)a))a)|a((b(aba|(aa|ab)b)|a(a(ab|bb)|b(aa|b(a|b))))b|((b(ab|bb)|aab)a|a(aa|ab)b)a)))a|((b((b(b(aa|b(a|b))|a(a(a|b)|ba))|a((a(a|b)|ba)a|(bb|ba)b))a|((aab|(bb|aa)a)a|(aaa|(bb|aa)b)b)b)|a(a(b(aa|ab)a|a(abb|b(aa|ab)))|b(((aa|b(a|b))b|aaa)b|((ab|ba)b|(aa|b(a|b))a)a)))a|((b((a(aa|ab)|b(aa|ba))a|(baa|a(aa|ba))b)|a((a(ab|ba)|bbb)a|(b(bb|ba)|a(ab|bb))b))b|(a(b(a(ab|bb)|b(aa|(a|b)b))|a(b(bb|aa)|a(aa|ba)))|b((b(a|b)(a|b)|a(aa|b(a|b)))b|((aa|b(a|b))b|(ab|bb)a)a))a)b)b)((b((a(b((ab|ba)a|(aa|ab)b)|a((a|b)(a|b)b|aaa))|b((abb|b(bb|ba))b|(b(bb|ba)|a(aa|ab))a))b|(((aba|(aa|ab)b)b|((aa|b(a|b))a|(aa|ba)b)a)b|(b(aa|b(a|b))(a|b)|a(aab|baa))a)a)|a(b(((b(ab|bb)|a(aa|ba))b|(b(ab|bb)|abb)a)b|(a((ab|ba)b|(aa|b(a|b))a)|b((aa|b(a|b))b|(aa|ab)a))a)|a((b(aba|(aa|ab)b)|a(a(ab|bb)|b(aa|b(a|b))))b|((b(ab|bb)|aab)a|a(aa|ab)b)a)))a|((b((b(b(aa|b(a|b))|a(a(a|b)|ba))|a((a(a|b)|ba)a|(bb|ba)b))a|((aab|(bb|aa)a)a|(aaa|(bb|aa)b)b)b)|a(a(b(aa|ab)a|a(abb|b(aa|ab)))|b(((aa|b(a|b))b|aaa)b|((ab|ba)b|(aa|b(a|b))a)a)))a|((b((a(aa|ab)|b(aa|ba))a|(baa|a(aa|ba))b)|a((a(ab|ba)|bbb)a|(b(bb|ba)|a(ab|bb))b))b|(a(b(a(ab|bb)|b(aa|(a|b)b))|a(b(bb|aa)|a(aa|ba)))|b((b(a|b)(a|b)|a(aa|b(a|b)))b|((aa|b(a|b))b|(ab|bb)a)a))a)b)b)((b(((a(abb|(bb|aa)a)|baaa)a|((a(bb|ba)|b(ab|bb))a|(b(b(a|b)|ab)|a(ab|bb))b)b)b|((abba|(b(ab|ba)|a(aa|ab))b)a|(a(bb|ba)(a|b)|b((aa|(a|b)b)a|(a(a|b)|ba)b))b)a)|a((a((bba|(a(a|b)|ba)b)a|(b(ab|ba)|a(aa|ab))b)|b(a((aa|ba)a|(a|b)(a|b)b)|b(aab|baa)))b|(a(((aa|(a|b)b)b|(bb|aa)a)a|((aa|b(a|b))a|(b(a|b)|ab)b)b)|b((abb|(b(a|b)|ab)a)b|(a(ab|ba)|bbb)a))a))b|(((b(a((bb|ba)a|aab)|b(b(b(a|b)|ab)|a(ab|bb)))|a(((a(a|b)|ba)b|(ab|bb)a)b|((ab|ba)a|(aa|ab)b)a))b|(a(b((bb|aa)b|bba)|a(a(aa|b(a|b))|b(aa|ba)))|b(a(a(aa|b(a|b))|b(b(a|b)|ab))|b(aba|abb)))a)a|(((b((aa|ba)b|(ab|bb)a)|a(b(a|b)(a|b)|a(aa|b(a|b))))a|((b(bb|aa)|a(aa|ba))a|(b(a(a|b)|ba)|aab)b)b)a|(b(b(b(aa|ab)|a(a(a|b)|ba))|a(bbb|bba))|a((abb|b(aa|ab))b|(b(bb|ba)|a(ab|bb))a))b)b)a)"



Wow, that's big. 


```julia
x = Regex("^"*itr[0]*"\$")
count(a->match(x,a)!==nothing, im)
```




    151



That was fast! Didn't even have time to blink. 

## Part 2
OK... To pretend that I'm doing something (while I'm panicking), read in and transform into a regular expression the test dataset for part 2. Look specifically at rules 42 and 31.


```julia
ir[0]
```




    "8 11"




```julia
sr,sm = readin("test2.txt")
str = transf1(sr)
r = buildregex(str,str[0])
str[0]= r
str[42],str[31]
```




    ("((b(a(bb|ab)|b(a|b)(a|b))|a(bbb|a(bb|a(a|b))))b|(((aa|ab)a|bbb)b|((a|b)a|bb)aa)a)", "(b(b(aba|baa)|a(b(ab|(a|b)a)|a(ba|ab)))|a(b((ab|(a|b)a)b|((a|b)a|bb)a)|a(bab|(ba|bb)a)))")



Now look at the original rule 0


```julia
sr[0]
```




    "8 11"



So: rule 0 is "8 then 11". New rule 8 is just "(42)+", and new 11 is any non-zero number of 42 followed by same number of 31. So, actually in order to match rule 0 we have to cut the message into some number of matching 42's, followed by a smaller (but non-zero) number of matching 31's. My biggest worry is "what if some substrings in the matches overlap". Like there is a substring that can be matched by either 31 or 42. If we are to greedy matching 42's, we may be left with nothing to match the 31, and incorrectly reject a matching string. Also, if either of the regexes can math different string lengths, there may be a problem, as in principle the match may not be unique and the ambiguity may disrupt further matching. 

But... Let's not worry about it until it actually happens. 


```julia
m=match(Regex("^"*str[42]),sm[2])
```




    RegexMatch("bbabb", 1="bbabb", 2="bbab", 3="bab", 4=nothing, 5="a", 6="b", 7=nothing, 8=nothing, 9=nothing, 10=nothing, 11=nothing, 12=nothing, 13=nothing, 14=nothing)




```julia
function match2(p1,p2,msg)
    # this function checks if msg matches a pattern of some number of matches of p1, followed by some smaller
    # number of matches of p2. I decided to use loops, because Julia's RegexMatch object does not tell me how
    # many times a (...)+ was matched. 
    r1 = Regex("^"*p1)    # ensure match is always at string begin.
    r2 = Regex("^"*p2)
    n1 = 0
    n2 = 0
    while (m=match(r1,msg)) !== nothing
        n1 += 1
        if m.match == msg
            msg = ""
            return n1,0,msg
        end
        msg = msg[(length(m.match)+1):end]
    end
    while (m=match(r2,msg)) !== nothing
        n2 += 1
        if m.match == msg
            msg = ""
            break
        end
        msg = msg[(length(m.match)+1):end]
    end
    # Return the two counts of matches and the unmatched "remnant"
    n1,n2,msg
end
match2(str[42],str[31],sm[1])
```




    (3, 1, "bbbbabababbbabbbbbbabaaaa")



OK, first test string does not match, because there is an unmatched remnant. Second one should match


```julia
match2(str[42],str[31],sm[2])
```




    (2, 1, "")




```julia
sm[2]
```




    "bbabbbbaabaabba"



A function that checks the match i.e. requires `n1>n2>0` and empty "remnant".


```julia
function sol2(r1,r2,msg)
    n1,n2,rest = match2(r1,r2,msg)
    return (n1>n2>0) && (rest=="")
end
sol2(str[42],str[31],sm[1])
```




    false




```julia
sol2(str[42],str[31],sm[2])
```




    true




```julia
count(s->sol2(str[42],str[31],s),sm)
```




    12



Aaaand the real thing:


```julia
count(s->sol2(itr[42],itr[31],s),im)
```




    386



AoC page tells me this is a correct answer, so either the data have been set so, that there are no overlaps between matches, or I was lucky. I don't really like the solution, because with more general input data it would simply break. But somehow I'm too lazy to think about a "completely correct" solution. Maybe something like matching the whole string against `^(r42)+(r31)+$` first, then breaking the original string into parts that matched the two regexes and counting the matches separately?
