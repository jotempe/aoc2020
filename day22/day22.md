# AoC 2020 day 22
When I was a kid, the [card game of war](https://en.wikipedia.org/wiki/War_(card_game)) fascinated me, because I observed, that the game is determined up to the ordering of the cards the round winner placed at the bottom of their deck. Everybody around kept telling me, that it was a game of pure chance, but I was wondering if it were possible to construct a strategy that maximises the winning odds by manipulating the ordering of those won cards. And even had a sort of a strategy. And dreamed of a computer, that would allow me to quickly count opponent's cards and find the optimal ordering based on what I know about their deck. Now I have computers, but that child's fascination is already gone, so I'm not going to make a card counting program and implement a strategy to win a card war. 

Part 1 looks easy, just implement the two decks as two queues and run. What worries me in advance is part 2, are we going to get some huge decks to play with?


```julia
using DataStructures
function readin(file)
    lines = readlines(file)
    d1 = Queue{Int}()
    d2 = Queue{Int}()
    cq = d1
    for l in lines
        if l == ""
            # Empty input line marks end of data for player 1
            cq = d2
        elseif startswith(l,"P")
            # Skip the "Player:" line 
            continue
        else
            enqueue!(cq,parse(Int,l))
        end
    end
    d1,d2
end
t1,t2 = readin("test.txt")
```




    (Queue{Int64}(Deque [[9, 2, 6, 3, 1]]), Queue{Int64}(Deque [[5, 8, 4, 7, 10]]))




```julia
function game(d1,d2)
    # Run the game until one deck is exhaused
    while !isempty(d1) && !isempty(d2)
        c1 = dequeue!(d1)
        c2 = dequeue!(d2)
        if c1>c2
            enqueue!(d1,c1)
            enqueue!(d1,c2)
        else
            enqueue!(d2,c2)
            enqueue!(d2,c1)
        end
    end
    d1,d2
end
game(t1,t2)            
```




    (Queue{Int64}(Deque [Int64[]]), Queue{Int64}(Deque [[3, 2, 10, 6, 8, 5, 9, 4, 7, 1]]))



The following function calculates the score of the winning deck. Note usage of `reverse_iter` to get card values in the queue in reverse order. 


```julia
function score(d)
    # Cound probably do it better using someting like 'accumulate'
    score = 0
    val = 1
    for c in reverse_iter(d)
        score += c*val
        val += 1
    end
    score
end
score(t2)
```




    306



Test OK.


```julia
i1,i2 = readin("input.txt")
game(i1,i2)
```




    (Queue{Int64}(Deque [[37, 32, 43, 36, 16, 2, 24, 20, 42, 22, 21, 12, 34, 7, 10, 6, 50, 18, 35, 11, 31, 15, 26, 4, 46, 41, 38, 5, 49, 25, 44, 1, 23, 19, 40, 30, 33, 27, 29, 17, 48, 14, 28, 8, 47, 13, 45, 9, 39, 3]]), Queue{Int64}(Deque [Int64[]]))



With my decks player 1 won. And their score is:


```julia
score(i1)
```




    32413



## Part 2
Understanding the rules from the description seems to be the hardest part of this puzzle.

Now it suddenly turns out, that representing decks as queues was not such a great idea after all, because they queues can't be indexed. Thus making a queue out of a subset of another one requires iteration, where with arrays is as simple as taking a slice. Pity.

The "stop on repeated configuration" rule is a bit of a problem, decided to implement "previous states" as a set of hashes of pairs of decks (sounds complicated, I know, just take both decks put them in a tuple and calculate the hash of that tuple). Finding an element in a set in Julia is fast, as we learned on day 17. Maybe one could actually just detect returning to the start configuration, not check all previous ones, but I see no quick argument for unitarity of the game and I'm not wasting time now on thinking about it.


```julia
function recgame(d1,d2)
    # Play the recursive version of the game. return true if player1 wins, plus both resulting decks
    prev = Set(hash((d1,d2)))
    while !isempty(d1) && ! isempty(d2)
        c1 = d1[1]
        c2 = d2[1]
        if c1<length(d1) && c2<length(d2)
            win,s1,s2 = recgame(d1[2:(c1+1)],d2[2:(c2+1)])
        else
            win = c1>c2
        end
        if win
            d1 = push!(d1[2:end],c1,c2)
            d2 = d2[2:end]
        else
            d1 = d1[2:end]
            d2 = push!(d2[2:end],c2,c1)
        end
        conf = hash((d1,d2))
        if conf in prev
            return true,d1,d2
        else
            push!(prev,conf)
        end
    end
    return isempty(d2),d1,d2
end
```




    recgame (generic function with 1 method)



Need a new data reading function, one that returns arrays, not queues. And anyhow, part 1 solution has destroyed the initial configurations of both decks.


```julia
function readin2(file)
    lines = readlines(file)
    d1 = []
    d2 = []
    cq = d1
    for l in lines
        if l == ""
            cq = d2
        elseif startswith(l,"P")
            continue
        else
            push!(cq,parse(Int,l))
        end
    end
    d1,d2
end
t1,t2 = readin2("test.txt")
```




    (Any[9, 2, 6, 3, 1], Any[5, 8, 4, 7, 10])




```julia
recgame(t1,t2)
```




    (false, Any[], Any[7, 5, 6, 2, 4, 1, 10, 8, 9, 3])



Test OK, run the competition input


```julia
i1,i2 = readin2("input.txt")
win,e1,e2 = recgame(i1,i2)
```




    (true, Any[47, 29, 26, 17, 35, 33, 13, 11, 45, 32  …  18, 15, 48, 42, 46, 7, 43, 36, 34, 5], Any[])



That was not exactly fast. 


```julia
score(e1)
```




    31596



## More
How to do it faster? One way would be to prove unitarity of the game, i.e. that the current configuration of both decks allows not only to construct the next state (this is obvious), but also to reconstruct the previous state in an unique way. I.e. each state has one and only one possible previous state. This would allow to compare each state to the initial state only when checking for infinite loops, instead comparing to the whole set of previous states. As I don't see a simple construction of the previous state (except in special cases), let's make an experiment, as a side effect it will tell us how often did we hit an infinite loop.


```julia
function recgame2(d1,d2)
    # Play the recursive version of the game. return true if player1 wins, plus both resulting decks
    inist = hash((d1,d2))
    prev = Set(inist)
    while !isempty(d1) && ! isempty(d2)
        c1 = d1[1]
        c2 = d2[1]
        if c1<length(d1) && c2<length(d2)
            win,s1,s2 = recgame(d1[2:(c1+1)],d2[2:(c2+1)])
        else
            win = c1>c2
        end
        if win
            d1 = push!(d1[2:end],c1,c2)
            d2 = d2[2:end]
        else
            d1 = d1[2:end]
            d2 = push!(d2[2:end],c2,c1)
        end
        conf = hash((d1,d2))
        if conf in prev
            if conf ≠ inist
                println("non-unitary!")
            else
                println("unitary?")
            end
            return true,d1,d2
        else
            push!(prev,conf)
        end
    end
    return isempty(d2),d1,d2
end

recgame2(i1,i2)
```




    (true, Any[47, 29, 26, 17, 35, 33, 13, 11, 45, 32  …  18, 15, 48, 42, 46, 7, 43, 36, 34, 5], Any[])



Great. So, we never hit an infinite loop, the whole hash business was unnecessary. How much did it slow us down?


```julia
@elapsed recgame(i1,i2)
```




    17.72944152




```julia
function recgamenohash(d1,d2)
    # Play the recursive version of the game. return true if player1 wins, plus both resulting decks
    while !isempty(d1) && ! isempty(d2)
        c1 = d1[1]
        c2 = d2[1]
        if c1<length(d1) && c2<length(d2)
            win,s1,s2 = recgame(d1[2:(c1+1)],d2[2:(c2+1)])
        else
            win = c1>c2
        end
        if win
            d1 = push!(d1[2:end],c1,c2)
            d2 = d2[2:end]
        else
            d1 = d1[2:end]
            d2 = push!(d2[2:end],c2,c1)
        end
    end
    return isempty(d2),d1,d2
end
recgamenohash(t1,t2)    # Run the game once, so that @ela
@elapsed recgamenohash(i1,i2)
```




    16.489675551



Very little (there is only one result for each in the rendered version of the notebook, but I did run it several times). The game itself took that long and I see no easy way to speed it up. I suspect now that a lot of time may go into copying the arrays in memory: the operation `d=d[2:end]` does, I suspect copy the array and throw away the original. So maybe some more clever data structure for the deck (a circular buffer?) would help with speed.

Another interesting question: how deep did we have to recurse?


```julia
function recgame(d1,d2,lvl)
    # Play the recursive version of the game. return true if player1 wins, plus both resulting decks
    if lvl>maxlvl
        global maxlvl = lvl
    end
    prev = Set(hash((d1,d2)))
    while !isempty(d1) && ! isempty(d2)
        c1 = d1[1]
        c2 = d2[1]
        if c1<length(d1) && c2<length(d2)
            win,s1,s2 = recgame(d1[2:(c1+1)],d2[2:(c2+1)],lvl+1)
        else
            win = c1>c2
        end
        if win
            d1 = push!(d1[2:end],c1,c2)
            d2 = d2[2:end]
        else
            d1 = d1[2:end]
            d2 = push!(d2[2:end],c2,c1)
        end
        conf = hash((d1,d2))
        if conf in prev
            return true,d1,d2
        else
            push!(prev,conf)
        end
    end
    return isempty(d2),d1,d2
end
```




    recgame (generic function with 2 methods)




```julia
maxlvl = 0
recgame(t1,t2,1)
maxlvl
```




    3




```julia
maxlvl=0
recgame(i1,i2,1)
maxlvl
```




    10



Wow, quite deep. 
