# AoC 2020 day 9
Part 1 is easy once you understood the description of the problem. Start with reading test and competition data sets into arrays.


```julia
function readlist(file)
    f=open(file)
    l = parse.(Int,readlines(f))
    close(f)
    l
end
inpd = readlist("input.txt")
tstd = readlist("test.txt")
```




    20-element Array{Int64,1}:
      35
      20
      15
      25
      47
      40
      62
      55
      65
      95
     102
     117
     150
     182
     127
     219
     299
     277
     309
     576



Check the length of the data, and look at the highest number. Yep, I have it in the editor, it is just a sanity check. 


```julia
length(inpd)
```




    1000




```julia
inpd[1000]
```




    100610451401986



Good that I have a 64-bit computer.

Let's brute force it. Function `sol1` finds the solution, for a given list of numbers and `step` is the count of previous numbers to consider.


```julia
function sol1(l,step)
    for i = (step+1):length(l)
        ok = false
        target = l[i]
        for j = (i-step):(i-2), k = (j+1):(i-1)
            if (l[j] + l[k]) == target
                ok = true
                break
            end
        end
        if !ok
            return target
        end
    end
    0
end
sol1(tstd,5)
```




    127



If would probably be slightly faster to have two separate loops in line 5 and compare the numbers `l[k]` against `target-l[j]`, but who cares, it is not production code.


```julia
sol1(inpd,25)
```




    1398413738



## Part 2
Should have stored the solution of part 1, but who knew? Anyhow, it is fast, so compute it again.


```julia
target = sol1(inpd,25)
```




    1398413738



So, here comes solution to part 2. Nothing to comment, really, it is a brute force checking all combinations again. 


```julia
function sol2(l,target)
    # should have returned also the index in sol1, but ok, find it now
    maxind = findall(x->x==target,l)[1]
    for i = 1:(maxind-2)
        s = l[i]
        for j = (i+1):maxind
            s += l[j]
            if s == target
                return(i, j, minimum(l[i:j])+maximum(l[i:j]))
            elseif s>target  # fortunately all numbers on the list are positive
                break
            end
        end
    end
    return 0
end
sol2(tstd,127)
```




    (3, 6, 62)



Finding the maximal loop index was not really required, for correct data the program should always stop before reaching it, and even if the loop runs beyond the target number, the inner loops will end almost immediately, because the numbers would generally be larger than target once we passed it on the list. Oh well, I'm too pedantic I suppose. 

Anyhow, here comes the solution


```julia
sol2(inpd,target)
```




    (522, 538, 169521051)



That was again too easy... That whole competition is too easy this year. [Aoc 2018](https://adventofcode.com/2018/) had on its [9-th day](https://adventofcode.com/2018/day/9) a marble game, that required already some thought, because in part 2 the list was huge and naïve solution by insertion/deletion of list elements became too slow. In [Aoc 2019](https://adventofcode.com/2019/day/9) it was completion of the Intcode processor, which at least required some more work to implement the address modes and all opcodes.
