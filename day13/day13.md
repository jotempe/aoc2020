# AoC 2020 day 13
First part looks rather trivial, wonder what role the x-es in the data will play in the second part. For now, start with reading the data in and keep it in text form for the second part. For the first part parse the numbers out.


```julia
function readin(file)
    f = open(file)
    l = readlines(f)
    close(f)
    l
end

function parsenum(line)
    out = []
    for w in split(line,',')
        if w ≠ "x"
            push!(out, parse(Int, w))
        end
    end
    out
end

testd = readin("test.txt")
```




    2-element Array{String,1}:
     "939"
     "7,13,x,x,59,x,31,19"




```julia
testts = parse(Int,testd[1])
testln = parsenum(testd[2])
```




    5-element Array{Any,1}:
      7
     13
     59
     31
     19



Now, for the solution: calculate the "wait time" for each bus (function `waitt`), find minimum. Integer division of the timestamp by bus line number gives the last departure, we need next. The problem statement does not specify what happens if you arrive exactly at the time of bus departure, assume it does not happen, i.e. wait time of 0 is not possible.


```julia
function waitt(ts,bus)
    tnext = (ts÷bus + 1) * bus
    return tnext-ts
end
wt = waitt.(testts, testln)
```




    5-element Array{Int64,1}:
      6
     10
      5
     22
     11




```julia
m = findmin(wt)
m[1] * testln[m[2]]
```




    295




```julia
inp =  readin("input.txt")
ints = parse(Int,inp[1])
inln = parsenum(inp[2])
wt = waitt.(ints, inln)
m = findmin(wt)
m[1] * inln[m[2]]
```




    136



## Part 2
Interesting, we need to find a smallest number $N$, such that:
$$ N \equiv p_i \mod b_i$$
for every bus $i$, where $b_i$ is the bus line number and $p_i$ is, no, not the position on the list. In order to have a bus arrive at the time equal to the position on the list, we need the remainder equal to the line number minus position in the list.

This problem is known in mathematics as the Chinese remainder problem. A related theorem, called the [Chinese Remainder Theorem](https://en.wikipedia.org/wiki/Chinese_remainder_theorem) states, that the problem has always a solution, as long as all the moduli $b_i$ are pairwise coprime. The proof of CRT is constructive, i.e. it is actually an algorithm for solving the problem. In principle I should implement that algorithm. But, fortunately, Julia appears to have a modular arithmetic package that contains a function solving the Chinese Remainder Theorem.

Let's start with building two lists: of line numbers and of their positions on the list (remember to count positions from 0)


```julia
function parse2(line)
    pos = 0
    ln = []
    ps = []
    for w in split(line,',')
        if w ≠ "x"
            push!(ln,parse(Int,w))
            push!(ps, pos)
        end
        pos += 1
    end
    ln,ps
end
testln, testps = parse2(testd[2])
```




    (Any[7, 13, 59, 31, 19], Any[0, 1, 4, 6, 7])



Good that all the bus line numbers are prime, it guarantees that they are also pairwise coprime. Let's see if it is true also for the competition data set


```julia
inln, inps = parse2(inp[2])
```




    (Any[13, 37, 401, 17, 19, 23, 29, 613, 41], Any[0, 7, 13, 27, 32, 36, 42, 44, 85])



Yes, all prime. Nice. Try to solve the test problem first. Start with changing the list of positions into a list of expected remainders. The `mod` function is needed because for some of the bus lines in the competition data set the desired arrival time is higher, than the bus line number. We'll therefore see extra buses arrive on that line before the expected one. 


```julia
rem(pos,bus) = mod(bus-pos,bus)
testrm = rem.(testps,testln)
```




    5-element Array{Int64,1}:
      0
     12
     55
     25
     12



Note that the solution to the CRT will also be the answer that we seek only if the above constructed remainder list contains a zero. Otherwise we'd have to find the smallest remainder and add to the solution, because we are asked about an actual bus departure time. Fortunately for the problem at hand the first remainder will be always zero if the corresponding bus line number is first on the list (i.e. the list starts with a number, not with "x"). It happens to be the case in our input.


```julia
using Mods
function solve(mods,rems)
    m = Mod(rems[1],mods[1])
    for i in 2:length(mods)
        m = CRT(m,Mod(rems[i],mods[i]))
    end
    m
end
solve(testln,testrm)
```




    Mod{3162341}(1068781)



The second number (the remainder) is what we are after, it is the smallest number satisfying the condition. We are ready for the solution.


```julia
inrm = rem.(inps,inln)
solve(inln, inrm)
```




    Mod{1044388633269293}(305068317272992)



I'm a but ashamed that I failed to write the `solve` function without an explicit loop, according to documentation the `CRT` function should accept more than two parameters, surely there must be a way to easily build a tuple of all the `Mod`s and pass it as parameter list to the `CRT` function. But I was in a hurry to get the solution. 
