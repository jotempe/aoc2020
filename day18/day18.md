# AoC 2020 day 18
Parsing expressions... Looks that all the numbers in the problems are single-digit, makes it easier to pick numbers.

Need something like a state machine: keep "current value" and "current op". States are 1 — 'awaiting first operand', 2 — 'awaiting operator', 3 — 'awaiting second operand'. in state 1 we just read a number, store in current state and go to state 2. In state 2 we read the operator, store in current state and go to state 3. In state 3 read number, execute operation, store result in current state, go to state 2. An opening parenthesis causes to push current state, current value and current op on stack and go to state 1. Closing parenthesis causes to pop state from the stack and, if the popped state is 2, perform operation using the last result as second operand. Otherwise just store current value in current state. In both cases go to state 2. End of line should only happen in state 2 and causes the function to return the current value.


```julia
using DataStructures
mutable struct State
    val ::Int
    op  ::Char
    st   ::Int
end
function calc(line)
    function performop(state,nv)
        # This function performs an operation using using operation and operand from state, and second operand in nv
        if state.op == '+'
            state.val = state.val + nv
        elseif state.op == '*'
            state.val = state.val * nv
        else
            println(" Zle performop")
        end
        state.st = 2
    end
    stack = Stack{State}()     # Not really needed, push and pop functions work on simple arrays too
    curst = State(0,' ',1)     # initial state: no first operand, no operator, state 1
    for c in line
        if c in '0':'9'        # All numbers in the puzzle are single-digit
            nv = parse(Int,c)
            if curst.st == 1
                curst.val = nv
                curst.st = 2
            elseif curst.st == 3
                performop(curst,nv)
            else
                println("Zle 1")
            end
        elseif c in ['+','*']
            if curst.st == 2
                curst.op = c
                curst.st = 3
            else
                println("Zle 2")
            end
        elseif c == '(' 
            if curst.st == 1 || curst.st == 3
                push!(stack,curst)
                curst = State(0,' ',1)    # initialise new state, as if we started a new line
            else
                println("Zle 3")
            end
        elseif c == ')' 
            if curst.st == 2
                v = curst.val             # this is the last computed result
                curst = pop!(stack)     
                if curst.st == 3
                    performop(curst,v)
                else
                    # This can happen only if the original expression started with opening parenthesis
                    curst.val = v
                    curst.st = 2
                end
            else
                println("Zle 4")
            end
        end
    end
    curst.val
end
function readin(file)
    f = open(file)
    l = readlines(f)
    close(f)
    l
end
testd = readin("test.txt")
calc(testd[1])
```




    71




```julia
calc.(testd)
```




    6-element Array{Int64,1}:
        71
        51
        26
       437
     12240
     13632



Went surprisingly smoothly. Here comes the solution.


```julia
inp = readin("input.txt")
sum(calc.(inp))
```




    4940631886147



## Part 2
OK, engineers have advantage here they have learned about building parsers, something I have to invent now. One idea is not to perform multiplications immediately, but push them on stack and wait. If the next operation is also a multiplication, we can safely perform the "hanging" one. Same with closing parenthesis, it performs the "hanging" multiplication. The catch is, that  we have to behave differently depending if there was an opening parenthesis or not before that previous hanging multiplication.

A solution to that problem would be to add a to add a 'level' to every operation on stack. Parentheses increase level. This allows to put on stack a delayed operation and safely check when in should be performed immediately, or only after a closing parenthesis. Still, stack manipulation is a headache.

A stupid idea is to preprocess the input line to add parentheses around all additions and then use what we already have. 

Or there is a recursive solution. put every found number and operator into a list. Encountering an opening parenthesis extracts from the input string everything up to the matching closing parenthesis and recurses to replace the expression with a number — result of parenthesised expression. Finally do all the operations in the list, replacing iteratively an operator and two neighbouring numbers by the operation result. To prioritize addition over multiplication perform first all additions in the list and then all multiplications. Do that.


```julia
function calc(line)
    expr = []         # This is the expression to compute as a list of tokens
    index = 1         # traces current position on the input line
    while index≤length(line)
        c = line[index]
        if c in '0':'9'
            nv = parse(Int,c)
            push!(expr,nv)
            index += 1
        elseif c == ' '
            index += 1
        elseif c in ['+','*']
            push!(expr,c)
            index += 1
        elseif c == '('
            ind = index+1
            lvl = 0
            # there is certainly a better way to locate matching parenthesis using regex, but I'm in a hurry
            while line[ind] ≠ ')' || lvl ≠ 0
                if line[ind] == '('
                    lvl += 1
                elseif line[ind] == ')'
                    lvl -=1
                end
                ind += 1
            end
            # recurse the extracted parenthesised expression
            push!(expr, calc(line[(index+1):(ind-1)]))
            index = ind+1
        end
    end
    if length(expr) > 1
        # First find all additions and replace them with sums of left and right elements
        while (i=findfirst(x->x=='+',expr)) !== nothing
            expr[i-1] = expr[i-1] + expr[i+1]
            deleteat!(expr,i)
            deleteat!(expr,i)
        end
        # Then do the same with multiplications
        while (i=findfirst(x->x=='*',expr)) !== nothing
            expr[i-1] = expr[i-1] * expr[i+1]
            deleteat!(expr,i)
            deleteat!(expr,i)
        end
    end
    expr[1]
end
calc.(testd)
```




    6-element Array{Int64,1}:
        231
         51
         46
       1445
     669060
      23340



Test OK, go.


```julia
inp = readin("input.txt")
sum(calc.(inp))
```




    283582817678281


