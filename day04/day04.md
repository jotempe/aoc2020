# AoC 2020 day 4
Hmmmm... Variable number of lines per passport, will have to read the file line by line. Storing the list of read passports looks like a good idea, because I suspect it may be needed in the second part. But, for a start let's make it just a list of strings with data of each passport represented as a single line, i.e. we combine input lines belonging to each passport into one string.


```julia
function readpasses(fname)
    f=open(fname)
    listp = []
    pass = ""
    for line in eachline(f)
        if line == ""
            # empty line causes adding current passport to the list, if it is not empty. Hope there are no empty passes!
            push!(listp, pass)
            pass = ""
        else
            if pass == ""
                pass = line
            else
                pass = pass * " " * line
            end
        end
    end
    # at end of input have to add the last passport to the list, if it is not empty
    if pass ≠ ""
        push!(listp, pass)
    end
    listp
end

testp = readpasses("test.txt")
```




    4-element Array{Any,1}:
     "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd byr:1937 iyr:2017 cid:147 hgt:183cm"
     "iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884 hcl:#cfa07d byr:1929"
     "hcl:#ae17e1 iyr:2013 eyr:2024 ecl:brn pid:760753108 byr:1931 hgt:179cm"
     "hcl:#cfa07d eyr:2025 pid:166559648 iyr:2011 ecl:brn hgt:59in"



In case you are wondering: the Julia manual devotes three paragraphs to explanations why `*` is a better choice than `+` as string concatenation operator, with references to abstract algebra. Some people are strange, and people who work with computers tend to be even stranger. 

Back to our problem: now make a list of required fields and a function that tests if all are present in the input. Looks like actual field values may not contain colon characters, so it is safe to just scan the strings for the field name substrings.


```julia
function validpass(pass,reqfl)
    valid = true
    for s in reqfl
        if !occursin(s,pass)
            valid=false
            break
        end
    end
    valid
end
req = ["byr:", "iyr:", "eyr:", "hgt:", "hcl:", "ecl:", "pid:"]

count(p->validpass(p,req),testp)
```




    2



Actually, after a few hours and some caffeine I noticed the obvious: that I could do the same in a much more compact way


```julia
function validpass(pass,reqfl)
    all(s->occursin(s,pass),reqfl)
end

count(p->validpass(p,req),testp)
```




    2



looks fine, go for it


```julia
passl = readpasses("input.txt")
count(p->validpass(p,req),passl)
```




    208



# Part 2
This is not difficult, this is tedious. Don't like this "puzzle" at all. It just tests if I can write regular expressions. Let's make a bunch of regular expressions testing the described conditions. OK, writing regexes to check that numbers are in range would definitely be an overkill, so let's use triples: regular expression, lower bound, upper bound. The strange case of `hgt:` tag (two sets of bounds, depending on units) will be handled in code, I'm in no mood to build anything fancy for such a primitive problem.


```julia
validators = [
    (r"\bbyr:(\d{4})\b","1920","2002"),
    (r"\biyr:(\d{4})\b","2010","2020"),
    (r"\beyr:(\d{4})\b","2020","2030"),
    (r"\bhgt:((\d{2})in|(\d{3})cm)\b","59","76"),
    (r"\bhcl:#[0-9a-f]{6}\b", nothing, nothing),
    (r"\becl:(amb|blu|brn|gry|grn|hzl|oth)\b", nothing, nothing),
    (r"\bpid:\d{9}\b", nothing, nothing)
]
```




    7-element Array{Tuple{Regex,Any,Any},1}:
     (r"\bbyr:(\d{4})\b", "1920", "2002")
     (r"\biyr:(\d{4})\b", "2010", "2020")
     (r"\beyr:(\d{4})\b", "2020", "2030")
     (r"\bhgt:((\d{2})in|(\d{3})cm)\b", "59", "76")
     (r"\bhcl:#[0-9a-f]{6}\b", nothing, nothing)
     (r"\becl:(amb|blu|brn|gry|grn|hzl|oth)\b", nothing, nothing)
     (r"\bpid:\d{9}\b", nothing, nothing)



The function below validates a single passport against a list of validators. All the validators must OK the pass for the function to return true. Note that the strange pattern for the `hgt:` field produces 3 captures on a successful match — the parenthesised fields in both branches of the alternative have their own capture positions. Thus we distinguish units by checking with capture contains something: if it is capture number 2, then height is in inches, otherwise 3 must be non-empty and height is expressed in centimetres.

Also note that we don't convert strings to numbers when checking ranges. That's because (fortunately) the number of digits in each of those range-validated fields is fixed, therefore ordering of numbers is the same as ordering of their string representations.


```julia
function validatepass(pass,validators)
    function validatefield(pass,validator)
        res = match(validator[1],pass)
        if res === nothing
            r = false
        elseif length(res.captures)==0
            r = true
        elseif length(res.captures)==1
            if validator[2] === nothing  # the case of the ecl: tag - regex did all the required checking
                r = true
            else
                r = (res.captures[1] ≥ validator[2] && res.captures[1] ≤ validator[3])
            end
        else
            if res.captures[3] === nothing
                val = res.captures[2]
                r = (val ≥ validator[2] && val ≤ validator[3])
            else
                val = res.captures[3]
                r = (val ≥ "150" && val ≤ "193")
            end
        end
        r
    end
    return all(v->validatefield(pass,v),validators)
end
```




    validatepass (generic function with 1 method)



You may wonder why `===` when comparing against `nothing`. Well, I did wonder too, seeing it in an example in the manual. So I looked it up. `===` means identity of objects: same type and same hash for immutable or same address for mutable ones. `==` is equality of values and is overloadable. In case of `nothing` it actually makes no difference if we use `==` or `===`, as all nothings refer to the same singleton object of type `Nothing`, so all are equal in the `===` sense. And the `==` operator falls back to `===` for most non-numeric types. But the spirit of this comparison is better represented by `===`.

We have 2 new test data sets, one with all passports valid, another one with all invalid. Check them


```julia
passval = readpasses("testv.txt")
passinv = readpasses("testin.txt")

all(p->validatepass(p,validators),passval)
```




    true




```julia
any(p->validatepass(p,validators),passinv)
```




    false



All that remains is to count hits on the competition dataset


```julia
count(p->validatepass(p,validators),passl)
```




    167



The author could have knocked it up a bit by introducing invalid (unknown) tags and declaring, that a passport containing a tag from outside the given set is automatically invalid. The existence of the `cid:` tag would at least make some sense then, it would be a tag that does not invalidate the passport, but is not required. For now that tag is useless, we just ignore everything from outside the required set and are done. Well, maybe they have some continuation in store for next days?
