# AoC 2020 day 12
Let's write a function that takes list of commands and location/direction and follows those commands to find the final location. Direction works in a "mathematical" way: 0 is east, 1 north, 2 west, 3 south. Let's hope all turns are in multiples of 90 degrees! `x` axis points east, `y` points north.


```julia
function follow(commands,x,y,dir)
    step = [(1,0),(0,1),(-1,0),(0,-1)] # arrays are 1-based, so step[1] is in direction 0!
    for c in commands
        # parse command into char and int - no regex needed
        cmd = c[1]
        num = parse(Int,c[2:end])
        if cmd == 'E'
            x += num
        elseif cmd == 'N'
            y += num
        elseif cmd == 'W'
            x -= num
        elseif cmd == 'S'
            y -= num
        elseif cmd == 'F'
            x,y = (x,y) .+ step[dir+1] .* num   # no solution without a vector operation
        elseif cmd == 'L'
            dir = mod(dir + num÷90, 4)
        elseif cmd == 'R'
            dir = mod(dir - num÷90, 4)
        else
            println("bad command $c")
        end
    end
    return x,y,dir
end
function readcomm(file)
    f = open(file)
    l = readlines(f)
    close(f)
    l
end
testc = readcomm("test.txt")     
```




    5-element Array{String,1}:
     "F10"
     "N3"
     "F7"
     "R90"
     "F11"



Let's try it


```julia
follow(testc,0,0,0)
```




    (17, -8, 3)



Great. Here comes the answer


```julia
inpc=readcomm("input.txt")
xf,yf,df = follow(inpc,0,0,0)
```




    (1615, -655, 0)




```julia
abs(xf)+abs(yf)
```




    2270



## Part 2
OK, my guess about the parameters was wrong, who cares. Write a new "navigation function". `xw, yw` are waypoint coordinates relative to the ship, helper function `rw` rotates the waypoint counter-clockwise by `ang` in units of 90 degrees (i.e.  `1` means 90 degree left, `3` 90 degree right).


```julia
function follow2(commands,x,y)
    function rw(xw,yw,ang)
        if mod(ang, 4) == 1
            ny = xw
            nx = -yw
        elseif mod(ang,4) == 2
            nx = -xw
            ny = -yw
        elseif mod(ang, 4) == 3
            nx = yw
            ny = -xw
        else
            println("Bad angle $ang")  # Assume we never get angle 0.
        end
        nx,ny
    end
    
    xw = 10
    yw = 1
    for c in commands
        cmd = c[1]
        num = parse(Int,c[2:end])
        if cmd == 'E'
            xw += num
        elseif cmd == 'N'
            yw += num
        elseif cmd == 'W'
            xw -= num
        elseif cmd == 'S'
            yw -= num
        elseif cmd == 'F'
            x += xw*num
            y += yw*num
        elseif cmd == 'L'
            xw,yw = rw(xw,yw,num÷90)
        elseif cmd == 'R'
            xw,yw = rw(xw,yw,-num÷90)
        end
    end
    x,y
end
follow2(testc,0,0)
```




    (214, -72)




```julia
x,y = follow2(inpc,0,0)
```




    (68489, 70180)



Both final coordinates are positive, no need for absolute values.


```julia
x+y
```




    138669



Yet another rather trivial puzzle in this year's Advent of Code is solved...
