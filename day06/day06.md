# AoC 2020 day 6

Today we are learning how to work with sets in Julia. Part 1 Looks easy. Start reading test and competition data in.


```julia
function readf(fname)
    f = open(fname)
    data = readlines(f)
    close(f)
    data
end
inpd = readf("input.txt")
test = readf("test.txt")
```




    15-element Array{String,1}:
     "abc"
     ""
     "a"
     "b"
     "c"
     ""
     "ab"
     "ac"
     ""
     "a"
     "a"
     "a"
     "a"
     ""
     "b"



This function produces a collection of sets, one set for each group, containing the union of all "yes" answers in a given group:


```julia
function mkgroups(data)
    grps = []
    grp = Set()
    for line in data
        if line == ""
            push!(grps,grp)
            grp = Set()
        else
            ans = Set(split(line,""))
            grp = union(grp, ans)
        end
    end
    if grp != []
        push!(grps,grp)
    end
    grps
end
mkgroups(test)
```




    5-element Array{Any,1}:
     Set(Any["c", "b", "a"])
     Set(Any["c", "b", "a"])
     Set(Any["c", "b", "a"])
     Set(Any["a"])
     Set(Any["b"])



Now the solution is easy:


```julia
function sol1(data)
    sol1 = sum(length.(mkgroups(data)))
end
sol1(test)
```




    11




```julia
sol1(inpd)
```




    6542



## Part 2
Again, seriously? Maybe it is intended as a punishment for people who didn't use sets to solve part 1? The function below again produces a list of sets, but now they are intersections of "yes" answer sets within a group. 


```julia
function mkgroups2(data)
    grps = []
    inigrp = Set(split("abcdefghijklmnopqrstuvwxyz",""))
    grp = inigrp
    for line in data
        if line == ""
            push!(grps,grp)
            grp = inigrp
        else
            ans = Set(split(line,""))
            grp = intersect(grp, ans)
        end
    end
    if grp != inigrp
        push!(grps,grp)
    end
    grps
end
mkgroups2(test)
```




    5-element Array{Any,1}:
     Set(SubString{String}["c", "b", "a"])
     Set{SubString{String}}()
     Set(SubString{String}["a"])
     Set(SubString{String}["a"])
     Set(SubString{String}["b"])



The solution is now identical, had I known it in advance, I'd have written it to accept a function as second parameter. Ok, do it, what the heck, show everybody that I know functions are first-class objects in Julia 😊


```julia
function sol2(data,fun)
    sum(length.(fun(data)))
end
sol2(test,mkgroups2)
```




    6




```julia
sol2(inpd,mkgroups2)
```




    3299



## More
Again, the puzzle itself was a bit disappointing, so let's learn some more Julia. First a simple question, do binary and updating operators on sets exist?


```julia
a = Set(["a","b","c"])
b = Set(["c","d","e"])
a+b
```


    MethodError: no method matching +(::Set{String}, ::Set{String})
    Closest candidates are:
      +(::Any, ::Any, !Matched::Any, !Matched::Any...) at operators.jl:538

    

    Stacktrace:

     [1] top-level scope at In[8]:3

     [2] include_string(::Function, ::Module, ::String, ::String) at .\loading.jl:1091



```julia
a*b
```


    MethodError: no method matching *(::Set{String}, ::Set{String})
    Closest candidates are:
      *(::Any, ::Any, !Matched::Any, !Matched::Any...) at operators.jl:538

    

    Stacktrace:

     [1] top-level scope at In[9]:1

     [2] include_string(::Function, ::Module, ::String, ::String) at .\loading.jl:1091


At least not the obvious ones. It is a bit difficult to search for it, because googling for "julia set" produces of course something completely different™. Maybe conventional math symbols?


```julia
a ∪ b
```




    Set{String} with 5 elements:
      "c"
      "e"
      "b"
      "a"
      "d"




```julia
a ∩ b
```




    Set{String} with 1 element:
      "c"




```julia
c = a
c ∪= b
```


    syntax: unexpected "="

    

    Stacktrace:

     [1] top-level scope at In[12]:2

     [2] include_string(::Function, ::Module, ::String, ::String) at .\loading.jl:1091


Yep, Unicode is it, but they did not define the updating versions of those operators. 

The second problem I have with the above solution is that it operates on sets of strings (actually sets of substrings, which are a different type in Julia), but it would be more efficient to work with sets of characters instead. How do I split string into characters, not substrings? Easily after some thought


```julia
s="abcafyec"
Set(s[1:end])
```




    Set{Char} with 6 elements:
      'f'
      'a'
      'c'
      'y'
      'e'
      'b'



And the last question: how to generate a range of characters? That is, can I write `Set("abcdefghijklmnopqrstuvwxyz"[1:end])` in a more clever way? Yep, the range operator works on chars:


```julia
length('a':'z')
```




    26



So, here comes a neater version of the same solution.


```julia
function grpunion(data)
    grps = []
    grp = Set()
    for line in data
        if line == ""
            push!(grps,grp)
            grp = Set()
        else
            grp = grp ∪ line[1:end]
        end
    end
    if grp != []
        push!(grps,grp)
    end
    grps
end

function grpinter(data)
    grps = []
    inigrp = Set('a':'z')
    grp = inigrp
    for line in data
        if line == ""
            push!(grps,grp)
            grp = inigrp
        else
            grp = grp ∩ line[1:end]
        end
    end
    if grp != inigrp
        push!(grps,grp)
    end
    grps
end

function solution(data,fun)
    sum(length.(fun(data)))
end

solution(inpd, grpunion)
```




    6542




```julia
solution(inpd, grpinter)
```




    3299



Note that it's not even needed to explicitly convert `line[1:end]` to a set, it is automagically promoted as an argument of a set operation. 
