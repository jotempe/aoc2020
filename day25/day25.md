# AoC 2020 day 25
An exercise in modular arithmetic. Apparently I am supposed to take a [discrete logarithm](https://en.wikipedia.org/wiki/Discrete_logarithm) of a given number with a given modulo. The procedure described looks suspiciously similar to the [Diffie-Hellman](https://en.wikipedia.org/wiki/Diffie%E2%80%93Hellman_key_exchange) public key cryptography protocol.

What do we know? The modulo 20201227 is prime. Apparently we could use something called [Pohlig–Hellman algorithm](https://en.wikipedia.org/wiki/Pohlig%E2%80%93Hellman_algorithm). But, as the modulo is not that large, the first question is how long would a brute-force solution take.

First, check with the test data if I understood the problem correctly. Use the Julia `Mods` package for modular arithmetic, introduced when solving the Chinese Remainder Theorem on day 13.


```julia
using Mods
p = 20201227
s0 = Mod(7,p)
s0^8,s0^11,s0^(8*11)
```




    (Mod{20201227}(5764801), Mod{20201227}(17807724), Mod{20201227}(14897079))



Great, that means I have understood correctly the description and reproduced the test numbers. Question: how fast is modular arithmetic in Julia, i.e. how long would it take to solve the problem by trial and error. Function `dl` takes a discrete logarithm of `n` modulo `p` by trying all exponents up to `p`.


```julia
function dl(n,p)
    base = Mod(7,p)
    i = 1
    v = base
    while v.val ≠ n
        v = v*base
        i+=1
    end
    i
end
dl(5764801,p)
```




    8



Fine, but that was a small number. See on my real data. No need to keep them on file, just put my both keys into code.


```julia
k1 = 8987316
k2 = 14681524
@elapsed pk1 = dl(k1,p)
```




    0.219384322




```julia
pk1
```




    2541700



Phew, that was fast! Two and a half million modular multiplications in time well below a second. Just continue then.


```julia
pk2 = dl(k2,p)
```




    4208732




```julia
((s0^pk1)^pk2).val
```




    15217943



And that concludes this year's too easy Advent of Code. Thanks to everyone. Season's Greetings, etc.
