# AoC 2020 day 14
And today we are training bitwise operations in Julia.

For the first part two basic ideas:
1. have each mask instruction produce two bitmasks, ones are written into "OR" mask, zeros into "AND" mask. 
2. memory addresses are big, memory array will be sparse, so implement memory as a dictionary.


```julia
function readprg(file)
    # This "read data" function appears in the majority of my solutions :)
    f = open(file)
    l = readlines(f)
    close(f)
    l
end

function makemasks(msk)
    # Analyse the mask string Prepare two masks: "ormask" contains ones where bits should be set, 
    # "andmask" has zeros where bits should be cleared
    ormsk = 0
    andmsk = 0xFFFFFFFFF   # the puzzle specifies 36 bits
    bit = 1
    for i in 36:-1:1
        if msk[i] == '1'
            ormsk |= bit
        elseif msk[i] == '0'
            andmsk &= ~bit
        end
        bit <<= 1
    end
    ormsk, andmsk
end

function runprg(prg)
    # This function executes the "program" and fills the "mem" dictionary that represents memory
    mem = Dict()
    ormsk = 0
    andmsk = 0xFFFFFFFFF
    for l in prg
        if (m=match(r"mask = (.+)",l)) !== nothing
            # Mask command, update masks
            ormsk, andmsk = makemasks(m.captures[1])
        else
            # Mem command, update memory
            m=match(r"mem\[(\d+)\] = (\d+)",l)
            adr = parse(Int, m.captures[1])
            val = parse(Int, m.captures[2])
            mem[adr] = Int((val | ormsk) & andmsk)
        end
    end
    mem
end
```




    runprg (generic function with 1 method)



Why "Int" in line 40? Well, apparently Julia treats hexadecimal constants as unsigned integers, and when it comes to dictionary keys, unsigned 1 is a different key from signed 1. To avoid problems, ensure all addresses are treated uniformly as signed integers.

So, having written the above functions, here comes the test (the `sumv` functions calculates the sum of values for a dictionary).


```julia
sumv(mem) = sum(values(mem))
testp = readprg("test.txt")
sumv(runprg(testp))
```




    165



And here comes the solution.


```julia
inp = readprg("input.txt")
sumv(runprg(inp))
```




    8570568288597



## Part 2
Congratulate myself on implementing memory as a dictionary. In part 1 the addresses were at most 5 decimal digits long, but now we have a full 36-bit address space to serve.

The biggest problem are the floating bits, how to approach that? We have to generate all combinations of these "floating" bits. Julia has a `Combinatorics` package that, among others, can generate all combinations of elements of a collection. So make a list of floating bit values, and add them to the base address. Also, we don't need the `andmsk` any more.


```julia
using Combinatorics
function makemasks2(msk)
    ormsk = 0
    flbits = []
    bit = 1
    for i in 36:-1:1
        if msk[i] == '1'
            ormsk |= bit
        elseif msk[i] == 'X'
            push!(flbits,Int(bit))
        end
        bit <<= 1
    end
    ormsk, flbits
end

function runprg2(prg)
    mem = Dict()
    ormsk = 0
    flbits = []
    for l in prg
        if (m=match(r"mask = (.+)",l)) !== nothing
            # Mask command, update mask and bitlist
            ormsk, flbits = makemasks2(m.captures[1])
        else
            m=match(r"mem\[(\d+)\] = (\d+)",l)
            adr = Int(parse(Int, m.captures[1]) | ormsk)
            val = parse(Int, m.captures[2]) 
            mem[adr] = val   # This is because the "combinations" function does not generate empty combination
            if length(flbits)>0
                for c in combinations(flbits)
                     mem[Int(adr+sum(c))] = val
                end
            end
        end
    end
    mem
end
```




    runprg2 (generic function with 1 method)



In this puzzle we have a separate test data set for part 2.


```julia
testp2=readprg("test2.txt")
sumv(runprg2(testp2))
```




    408



Wrong! why?


```julia
runprg2(testp2)
```




    Dict{Any,Any} with 12 entries:
      91 => 100
      26 => 1
      35 => 1
      34 => 1
      29 => 1
      58 => 100
      59 => 100
      37 => 1
      28 => 1
      90 => 100
      27 => 1
      36 => 1



Those high addresses (90 and 91) should not be there... OK, I know, the base address may have some of those "floating bits" already set, so we should not add each combination, but XOR it with the base address. Here is the correct version:


```julia
function runprg2(prg)
    mem = Dict()
    ormsk = 0
    flbits = []
    for l in prg
        if (m=match(r"mask = (.+)",l)) !== nothing
            # Mask command, update mask and bitlist
            ormsk, flbits = makemasks2(m.captures[1])
        else
            m=match(r"mem\[(\d+)\] = (\d+)",l)
            adr = Int(parse(Int, m.captures[1]) | ormsk)
            val = parse(Int, m.captures[2]) 
            mem[adr] = val   # This is because the "combinations" function does not generate empty combination
            if length(flbits)>0
                for c in combinations(flbits)
                     mem[Int(adr ⊻ sum(c))] = val
                end
            end
        end
    end
    mem
end
sumv(runprg2(testp2))
```




    208




```julia
sumv(runprg2(inp))
```




    3289441921203



Not that difficult, I liked the yesterday's puzzle more. But at least I have learned a new Julia package. And that thing about dictionary keys. 
